#ifndef nEXODigiAnalysis_hh
#include "nEXODigiAnalysis.hh"
#endif

#include "nEXOChargeReadoutDigitize.hh"
#include<iostream>
#include "TMath.h"
#include "TGraph.h"

using namespace std;

nEXOChargeReadoutDigitize* nEXOChargeReadoutDigitize::fInstance = NULL;

nEXOChargeReadoutDigitize::nEXOChargeReadoutDigitize()
{
  fFastDiffusionSim = false;
  fInductionSim = false;
  fElectronLifetime = 10.0e3*CLHEP::microsecond; // us
  fDiffusionCoef = 1e2*(CLHEP::cm*CLHEP::cm)/CLHEP::second; // cm^2/s
  fDriftVelocity = DRIFT_VELOCITY;
  fSeed = 1;
  fRand = TRandom3(fSeed);
  fDebug = false;
  fPrintEvent = 1;
  fSamplingInterval = 0.5*CLHEP::microsecond; // us
  fSaveWaveform = false;
  fApplyThreshold = true;
  fNoiseSigma = 0; // noise on sigle channel: 0 electrons
  fInductionZoffset = 0.;
  fInductionToffset = 0.;
  fMinimumZ = 1e6; // maximum Z of PCDs in this MC event
  fMaximumZ = -1e6; // maximum Z of PCDs in this MC event
  fADCGain = 50.; // 50 electrons per ADC
  fSamplingSeqZ.clear();
  fPCDZ = 0;
  fZSeqTemplate = 0;
  fEventNum = 0;
}

void nEXOChargeReadoutDigitize::Reset() 
{
}

void nEXOChargeReadoutDigitize::GeneratePCDs(Long64_t evtEntry, nEXOEventData* ED)
{
  // GeneratePCDs produces a collection of PCDs arrived at the anode plane,
  // it simulates the diffusion and transportation of thermal electrons
  // that generated in the ionziation processes
  ClearPCDs();

  //if(evtEntry%100==0) 
    std::cout << "------ Processing event " << evtEntry << " ------" << std::endl; 
  fEventNum++;

  nEXODigiAnalysis::GetInstance()->GetEntry(evtEntry);
  Double_t teX=0., teY=0., teZ=0., teE=0.;
  Double_t DriftTime = 0.;

  Int_t NTE = nEXODigiAnalysis::GetInstance()->GetNumTE();

  if(fFastDiffusionSim==false) {
    for (Int_t i=0; i<NTE; i++) {
      // Get each thermal electron's position
      nEXODigiAnalysis::GetInstance()->GetTE(i, teE, teX, teY, teZ);
      // calculate drift time, NOTE: teZ should be in mm unit
      DriftTime = (nEXODigiAnalysis::GetInstance()->GetAnodeZ()-teZ*CLHEP::mm)/fDriftVelocity;

      // calculate attenuation 
      Double_t attn = TMath::Exp(-DriftTime/fElectronLifetime);
      if(fRand.Uniform()>attn) {
        if(fDebug && evtEntry<fPrintEvent) std::cout << "electron be absorbed." << std::endl;
        continue; // electron disappeared
      }

      // calculate diffusion, sigma = sqrt(2*D*t)
      Double_t sigmaDiffusion = TMath::Sqrt(2. * fDiffusionCoef * DriftTime);
    //  Double_t DiffusionSpread = TMath::Abs(fRand.Gaus(0, sigmaDiffusion));
    //  Double_t phi = fRand.Uniform(0, 2*TMath::Pi());
    //  Double_t dx = TMath::Sin(phi)*DiffusionSpread;
    //  Double_t dy = TMath::Cos(phi)*DiffusionSpread;
		Double_t dx=fRand.Gaus(0, sigmaDiffusion);
		Double_t dy=fRand.Gaus(0, sigmaDiffusion);
      if(fDebug && evtEntry<fPrintEvent) {
        std::cout << "(X,Y,Z): (" << teX << ", " << teY << ", " << teZ << ")" 
                  << " drift time (us): " << DriftTime/CLHEP::microsecond
                  << " sigmaDiffusion (mm): " << sigmaDiffusion/CLHEP::mm
        //          << " fDiffusionSpread (mm): : " << DiffusionSpread/CLHEP::mm
                  << std::endl;
      }

      AddTEPoint( teX + dx, teY + dy, teZ, DriftTime, 0);
    }
  }
  else {
    for (Int_t i=0; i<NTE; i++) {
      // Get each thermal electron's position
      nEXODigiAnalysis::GetInstance()->GetTE(i, teE, teX, teY, teZ);
      DriftTime = (nEXODigiAnalysis::GetInstance()->GetAnodeZ()-teZ*CLHEP::mm)/fDriftVelocity;

      AddTEPoint( teX, teY, teZ, DriftTime, 0);
    }

    // first group thermal electrons into PCDs, 
    // then simulate the diffusion and attenuation
    PCDMap::iterator iter;
    for(iter= fPCDMap.begin(); iter!=fPCDMap.end(); iter++) {
      nEXOMCPixelatedChargeDeposit* pcd = iter->second;
      nEXOCoordinates pcdCenter = pcd->GetPixelCenter();
      
      // attenuation due to finite electron lifetime
      DriftTime = (nEXODigiAnalysis::GetInstance()->GetAnodeZ()-pcdCenter.GetZ()*CLHEP::mm)/fDriftVelocity; // us
      pcd->fTotalTE = pcd->fTotalTE * TMath::Exp(-DriftTime/fElectronLifetime);
    }

    // FIXME: add fast diffuse simulation below
    
  } // end if(fFastDiffusionSim==false) 

  GroupPCDsForInductionCalc();

  //if(fDebug) PrintPCDMap();
  PrintPCDMap();
}

void nEXOChargeReadoutDigitize::Digitize(nEXOEventData* ED)
{
  ClearHitMap();
  // Simulate the charge collection signal
  // Determine which channels the PCDs will hit
  PCDMap::iterator iter;
  Int_t tileId=0, localId=0;
  fMinimumZ = 1e6;
  fMaximumZ = -1e6;
  Double_t tmax=-1e9, tmin=1e9;
  std::cout << "fPCDMap.size(): " << fPCDMap.size() << std::endl;
  for(iter= fPCDMap.begin(); iter!=fPCDMap.end(); iter++) {
    nEXOMCPixelatedChargeDeposit* pcd = iter->second;
    nEXOCoordinates pcdCenter = pcd->GetPixelCenter();
    if( pcdCenter.GetZ()<fMinimumZ ) fMinimumZ = pcdCenter.GetZ();
    if( pcdCenter.GetZ()>fMaximumZ ) fMaximumZ = pcdCenter.GetZ();

    Bool_t isFound=nEXOChannelMap::GetInstance()->FindHitChargeChannel(pcdCenter, tileId, localId);
    if(!isFound) {
      std::cout << "Warning! no hit channel is found!" << std::endl;
      std::cout << "tileId: " << tileId << "localId: " << localId;
      std::cout << "pcdCenter: (" <<pcdCenter.GetX()<<","<<pcdCenter.GetY()<<","<<pcdCenter.GetZ()<<")"<< std::endl;
      continue;
    }

    if(fDebug) {
      std::cout << "Found tileId: " << tileId << " localId: " << localId;
      std::cout << " pcdCenter: (" <<pcdCenter.GetX()<<","<<pcdCenter.GetY()<<","<<pcdCenter.GetZ()<<")"<< std::endl;
    }

    nEXOChargeReadout* readout = FindOrCreateChargeReadout(tileId, ED);
    Int_t chanId = nEXOChannelMap::GetInstance()->GetChannelId(tileId, localId);
    nEXOChargeElecChannel* chargeCh = readout->FindOrCreateChargeChannel(chanId);

    Double_t preCharge = chargeCh->GetChannelCharge();
//    Double_t preCharge = 0;
   chargeCh->SetNTE( pcd->fTotalTE + chargeCh->GetNTE() );
	 if(TMath::Abs(preCharge)<1e-9)
	    chargeCh->SetChannelCharge( preCharge + pcd->fTotalTE + fRand.Gaus(0,fNoiseSigma));
	 else
		 chargeCh->SetChannelCharge( preCharge + pcd->fTotalTE);
    // Hit Time
    Double_t time =  pcdCenter.GetT(); // CLHEP::microsecond
	 if(tmax<time)	tmax = time;
	 if(tmin>time)	tmin = time;
  //  time += fRand.Uniform(-0.1*CLHEP::microsecond, 0.1*CLHEP::microsecond); // smear the hit time
    if( chargeCh->GetChannelFirstTime()>time) chargeCh->SetChannelFirstTime( time );
    if( chargeCh->GetChannelLatestTime()<time) chargeCh->SetChannelLatestTime( time );

    chargeCh->SetChannelTime( time );
  }
  for(Int_t tileId=0;tileId<nEXOChannelMap::GetInstance()->GetTileNum();tileId++){
	  nEXOChargeReadout* readout = FindOrCreateChargeReadout(tileId, ED);

	  for(Int_t localId=0;localId<nEXOChannelMap::GetInstance()->GetChannelNumOnTile();localId++){
      // ignore the channels that don't have charge deposit
		  Int_t chanId = nEXOChannelMap::GetInstance()->GetChannelId(tileId, localId);
      nEXOChargeElecChannel* chargeCh = const_cast<nEXOChargeElecChannel*>(readout->FindChargeChannel(chanId));
      // if find charge channel, tag this channel if its charge is below threshold
		  if(chargeCh!=NULL)	{
        //std::cout << "NumCh, channel charge, NTE: "
        //          << readout->GetNumChargeChannels() << ", " 
        //          << chargeCh->GetChannelCharge() << ", " 
        //          << chargeCh->GetNTE() << std::endl;
        if(chargeCh->GetChannelCharge()<=fNoiseSigma*5) {
			    chargeCh->SetNoiseTag(1);
        }
        //std::cout << "after remove channel: " << readout->GetNumChargeChannels()<< std::endl;
        continue;
      }

      // otherwise, create new channel if noise is above threshold
		  Double_t noise = fRand.Gaus(0,fNoiseSigma);
		  if(noise>fNoiseSigma*5){
			  cout<<"noise error!"<<endl;
		    chargeCh = readout->FindOrCreateChargeChannel(chanId);
			  chargeCh->SetChannelCharge(noise);
		    Double_t noisetime = fRand.Uniform(tmin,tmax);
			  chargeCh->SetChannelTime(noisetime);
			  chargeCh->SetChannelFirstTime(noisetime);
			  chargeCh->SetChannelLatestTime(noisetime);
			  chargeCh->SetNoiseTag(1);
			  chargeCh->SetNTE(0);
      }
	  }
    //if(readout->GetNumChargeChannels()==0) { 
    //  fHitMap.erase(tileId); 
    //  std::cout << "before remove: " << ED->GetNumChargeReadouts() << std::endl;
    //  ED->RemoveChargeReadout(readout);
    //  std::cout << "after remove: " << ED->GetNumChargeReadouts() << std::endl;
    //  delete readout; 
    //}
  }

  Double_t MaximumT = nEXODigiAnalysis::GetInstance()->GetAnodeZ() - fMinimumZ;
  Double_t MinimumT = nEXODigiAnalysis::GetInstance()->GetAnodeZ() - fMaximumZ;
  MaximumT = MaximumT/(fDriftVelocity/(CLHEP::mm/CLHEP::microsecond));
  MinimumT = MinimumT/(fDriftVelocity/(CLHEP::mm/CLHEP::microsecond));
  std::cout << "fMaximumZ: " << fMaximumZ << ", " << MaximumT << std::endl;
  std::cout << "fMinimumZ: " << fMinimumZ << ", " << MinimumT << std::endl;

  if(fDebug) PrintHitMap();

  // Simulate the charge induction signal
  if(fInductionSim==true) {
    // Determine the time sequences of waveform sampling
    if(!fZSeqTemplate) SetSamplingZSeqTemplate();
    DetermineSamplingSequence(fMaximumZ, fMinimumZ);
    if( fEventNum ) {
      cout << "fSamplingSeqZ.size(): " << fSamplingSeqZ.size() << endl;
      for(size_t i=0; i<fSamplingSeqZ.size(); i++) {
        cout << fSamplingSeqZ[i]/(fDriftVelocity/(CLHEP::mm/CLHEP::microsecond))  << ", ";
      }
      cout << endl;
    }
    
    // nly calcualte the tiles which have real charge collection
    HitMap::iterator ccIt;
    for(ccIt=fHitMap.begin(); ccIt!=fHitMap.end(); ccIt++) {
      nEXOChargeReadout* cro = ccIt->second;

      for(Int_t k=0; k<nEXOChannelMap::GetInstance()->GetChannelNumOnTile(); k++) {
        bool isCalcInduction = false;
        Int_t chanId = nEXOChannelMap::GetInstance()->GetChannelId(cro->GetTileId(), k);
        if( cro->FindChargeChannel(chanId)!=NULL ) {
          // if this channel has real charge collection
          isCalcInduction = true;
          //if(fApplyThreshold==true) {
          //  const nEXOChargeElecChannel* cCh = cro->FindChargeChannel(chanId);
          //  if(cCh->fChannelCharge<3*fNoiseSigma) isCalcInduction = true;
          //}
        }
        else {
          // if this channel has NO charge collection, but close to a collection channel
          // create ElecChannel and calculate the induction signal
        //  TVector3 chanPos = nEXOChannelMap::GetInstance()->GetChannelPosition(chanId);
        //  for(iter= fPCDMap.begin(); iter!=fPCDMap.end(); iter++) {
        //    nEXOMCPixelatedChargeDeposit* pcd = iter->second;
        //    nEXOCoordinates pcdCenter = pcd->GetPixelCenter();
        //    if(nEXOChannelMap::GetInstance()->GetChannelType(k)=="X") {
        //      // if local channel type is X- channel
        //      double distTmpX = TMath::Abs(chanPos.x() - pcdCenter.GetX());
        //      double distTmpY = TMath::Abs(chanPos.y() - pcdCenter.GetY());
        //      if(distTmpX<3*nEXOChannelMap::GetInstance()->GetPadSize() && distTmpY<TILE_SIZE/2) { isCalcInduction = true; }
        //    }
        //    if(nEXOChannelMap::GetInstance()->GetChannelType(k)=="Y") {
        //      // if local channel type is Y- channel
        //      double distTmpX = TMath::Abs(chanPos.x() - pcdCenter.GetX());
        //      double distTmpY = TMath::Abs(chanPos.y() - pcdCenter.GetY());
        //      if(distTmpY<3*nEXOChannelMap::GetInstance()->GetPadSize() && distTmpX<TILE_SIZE/2) { isCalcInduction = true; }
        //    }
        //  }
        }

        if(isCalcInduction==true) {
          //std::cout << "calc waveform for channel: " << chanId << std::endl;
          nEXOChargeElecChannel* chargeCh = cro->FindOrCreateChargeChannel(chanId);
          CalcWaveformOnChannel(chargeCh);
        }
      }
    }
  }
  
  // Combine the collection and induction signals
  
  // After Digitization, Output to root file, then clear the HitMap
  
  // assign the charge readout to the nEXOEventData object
  //HitMap::iterator ccIt;
  //for(ccIt=fHitMap.begin(); ccIt!=fHitMap.end(); ccIt++) {
  //  (ccIt->second);
  //}

}

void nEXOChargeReadoutDigitize::SetSamplingZSeqTemplate()
{
  std::vector<Double_t> ZBinsVec;

  ZBinsVec.push_back(1350.);
  ZBinsVec.push_back(1300.);
  ZBinsVec.push_back(1100.);
  ZBinsVec.push_back(900.);
  ZBinsVec.push_back(700.);
  ZBinsVec.push_back(500.);
  ZBinsVec.push_back(300.);
  ZBinsVec.push_back(100.);
  ZBinsVec.push_back(80.);
  ZBinsVec.push_back(60.);
  ZBinsVec.push_back(40.);
  ZBinsVec.push_back(20.);
  ZBinsVec.push_back(15.);
  ZBinsVec.push_back(10.);
  Double_t dZ = fDriftVelocity/(CLHEP::mm/CLHEP::microsecond)*(fSamplingInterval/CLHEP::microsecond);
  Double_t tmpZ = 10.;
  while(tmpZ-dZ > 0.) {
    ZBinsVec.push_back(tmpZ-dZ);
    tmpZ = tmpZ - dZ;
  }
  ZBinsVec.push_back(0.);

  Int_t NBin = ZBinsVec.size();
  Double_t ZBins[200];
  cout << "ZBins: ";
  for(Int_t i=0; i<NBin; i++) {
    ZBins[i] = ZBinsVec[NBin-1-i];
    cout << ZBins[i] << ", ";
  }
  cout << endl;

  fZSeqTemplate = new TH1F("ZSeqTemplate", "", NBin-1, ZBins);
}

void nEXOChargeReadoutDigitize::DetermineSamplingSequence(Double_t maxZ, Double_t minZ)
{
  Double_t anodeZ = nEXODigiAnalysis::GetInstance()->GetAnodeZ();
  fSamplingSeqZ.clear();
  fSamplingSeqZ.push_back(maxZ);

  int bin = fZSeqTemplate->FindBin(anodeZ-maxZ);
  for(int i=bin; i>=1; i--) {
    fSamplingSeqZ.push_back( anodeZ - fZSeqTemplate->GetBinLowEdge(i) );
  }

  // Determine the sampling points between (minZ, maxZ)
  // the sampling points should be fine enough to keep the feature of the waveform
  Int_t currentBin = fPCDZ->FindBin(maxZ);
  Int_t minBin = fPCDZ->FindBin(minZ);
  Double_t deltaZ = 0.;
  while(currentBin>minBin) {
    Int_t idx = 0;
    Int_t j = currentBin-1;
    while( idx == 0 ) {
      if( fPCDZ->GetBinContent(j)!=0 )  { 
        idx = j; 
        deltaZ = fPCDZ->GetBinCenter(currentBin) - fPCDZ->GetBinCenter(j); 
      }
      j--;
    }

    Double_t currentZ = fSamplingSeqZ[fSamplingSeqZ.size()-1];

    bin = fZSeqTemplate->FindBin(deltaZ);
    for(int i=bin; i>=1; i--) {
      fSamplingSeqZ.push_back( currentZ + deltaZ - fZSeqTemplate->GetBinLowEdge(i) );
    }
    currentBin = idx;
  }
  //if(currentBin == minBin)  fSamplingSeqZ.push_back(anodeZ + (maxZ-minZ));

  for(size_t i=0; i<fSamplingSeqZ.size(); i++) {
    deltaZ = fSamplingSeqZ[i] - maxZ;
    fSamplingSeqZ[i] = deltaZ;
  }
}

void nEXOChargeReadoutDigitize::CalcWaveformOnChannel(nEXOChargeElecChannel* chargeCh){

  std::vector<Double_t> wf;
  wf.clear();
  for(Int_t i=0; i<500; i++) { wf.push_back(0.); }

  PCDMap::iterator iter;
  std::vector<TVector3>* padsCoord = 0;

  Int_t chanId = chargeCh->fChannelId;
  Double_t totalIonQ = 0.;
  for(iter= fInductionPCDMap.begin(); iter!=fInductionPCDMap.end(); iter++) {
    // loop the PCDs and calculate the induction 
    nEXOMCPixelatedChargeDeposit* pcd = iter->second;
    nEXOCoordinates pcdCenter = pcd->GetPixelCenter();

    padsCoord = nEXOChannelMap::GetInstance()->GetPadsCoord(chanId);
    if(padsCoord==0) {
      cout << "tileId, LclId : " << nEXOChannelMap::GetInstance()->GetTileId(chanId) 
           << ", " << nEXOChannelMap::GetInstance()->GetLocalId(chanId) << endl;
    }
    
    Double_t iniZ = nEXODigiAnalysis::GetInstance()->GetAnodeZ() - pcdCenter.GetZ();

    for(size_t i=0; i<padsCoord->size(); i++) {
      //std::cout << "chanId, padPos: "  << chanId << ", (" << (*padsCoord)[i].x() << ", " 
      //          << (*padsCoord)[i].y() << ", " << (*padsCoord)[i].z() << ")" << std::endl;

      Double_t xpad = (*padsCoord)[i].x();
      Double_t ypad = (*padsCoord)[i].y();
      Double_t xq = pcdCenter.GetX();
      Double_t yq = pcdCenter.GetY();

      // there is an rotation on the axes
      // due to the coordinates definition and orientation of Pads
      Double_t dX = TMath::Abs( ((xpad - xq) + (ypad - yq))/TMath::Sqrt(2) );
      Double_t dY = TMath::Abs( ((ypad - yq) - (xpad - xq))/TMath::Sqrt(2) );

      Int_t xId = nEXOFieldWP::GetInstance()->GetXBin(dX);
      Int_t yId = nEXOFieldWP::GetInstance()->GetYBin(dY);
      
      if(xId>150 or yId>150) {
        continue;
        // This is to speed up the simulation, ignore the far PCDs
        // one can comment it out to get more precise calculation
      }

      TH1F* h = nEXOFieldWP::GetInstance()->GetHist(xId, yId);
      Double_t qIon = h->Interpolate( iniZ );
      totalIonQ += qIon/1e5 * pcd->fTotalTE;

      for(size_t j=0; j<fSamplingSeqZ.size(); j++) {
        Double_t deltaZ = fSamplingSeqZ[j];
        Double_t amplitude = 0.;
        // Do extrapolation if the PCD is very close to anode
        if(iniZ-deltaZ>=0.05) amplitude = h->Interpolate( iniZ - deltaZ );
        else if(iniZ-deltaZ>=0.)  
          amplitude = h->GetBinContent(1)*(1-(iniZ-deltaZ)/0.05) + h->GetBinContent(2)*(iniZ-deltaZ)/0.05;
        else amplitude = h->GetBinContent(1);

        wf[j] += (amplitude - qIon)/1e5 * pcd->fTotalTE;
        // 1e5 is the scaling factor for the field map
      }
    }
  }

  if(chargeCh->fWaveformAmplitude.size()!=0) chargeCh->fWaveformAmplitude.clear();

  bool isAtEdge = false;
  if(nEXOChannelMap::GetInstance()->GetLocalId(chanId)==0  ||
     nEXOChannelMap::GetInstance()->GetLocalId(chanId)==nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2-1 ||
     nEXOChannelMap::GetInstance()->GetLocalId(chanId)==nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2 ||
     nEXOChannelMap::GetInstance()->GetLocalId(chanId)==nEXOChannelMap::GetInstance()->GetChannelNumOnTile()-1 ) {
    isAtEdge = true;
  }

  Double_t samplingTime = 0.;
  for(size_t j=0; j<fSamplingSeqZ.size(); j++) {
    samplingTime = fSamplingSeqZ[j]/( fDriftVelocity/(CLHEP::mm/CLHEP::microsecond) );
    chargeCh->fWaveformTime.push_back( samplingTime );
    //if(isAtEdge==true)
    //{
    //  // channel at the tile edge only has half area
    //  // induction is also reduced by half -- temporary solution
    //  cout << "isAtEdge! " << endl;
    //  chargeCh->fWaveformAmplitude.push_back( wf[j]/2 );
    //}
    //else {
      chargeCh->fWaveformAmplitude.push_back( wf[j] );
    //}
  }
  // Fill the last two points
  chargeCh->fWaveformTime.push_back( samplingTime + 0.5 );
  chargeCh->fWaveformAmplitude.push_back( chargeCh->GetNTE() - totalIonQ);
  chargeCh->fWaveformTime.push_back( samplingTime + 20. );
  chargeCh->fWaveformAmplitude.push_back( chargeCh->GetNTE() - totalIonQ);
}

nEXOChargeReadout* nEXOChargeReadoutDigitize::FindOrCreateChargeReadout(Int_t tileId, nEXOEventData* ED)
{
  // Use channel Map to determine which channel get hit
  // Find if it's in the exsiting HitMap
  HitMap::iterator iter = fHitMap.find(tileId);
  if ( iter != fHitMap.end() ) { 
    return static_cast<nEXOChargeReadout*>(iter->second);
  }
  else {
    //nEXOChargeReadout* readout = new nEXOChargeReadout(tileId);
    nEXOChargeReadout* readout = ED->GetNewChargeReadout();
    readout->SetTileId(tileId);
    FillHitMap(readout);
    return readout;
  }
}

void nEXOChargeReadoutDigitize::SetNoiseModel()
{
  // Sampling the noise power spectra
}

void nEXOChargeReadoutDigitize::AddTEPoint(double xpos, double ypos, double zpos, double time, Int_t keyType)
{
    // Add a charge point at a given x, y, z position (in mm), 
    // at a certain time (in us)
    const nEXOCoordinates Coord(xpos, ypos, zpos, time);
    nEXOMCPixelatedChargeDeposit* pc = FindOrCreatePCD(Coord, keyType);
    pc->fTotalTE += 1;
}

nEXOMCPixelatedChargeDeposit* nEXOChargeReadoutDigitize::FindOrCreatePCD(const nEXOCoordinates& coord, Int_t keyType)
{
  const nEXOCoordinateKey& MyKey = coord.GetCoordinateKey(keyType);
  PCDMap::iterator iter = fPCDMap.find( MyKey );
  if ( iter != fPCDMap.end() ) { 
    return static_cast<nEXOMCPixelatedChargeDeposit*>(iter->second);
  }
  else {
    //nEXOMCPixelatedChargeDeposit* ChargeDeposit = new nEXOMCPixelatedChargeDeposit(coord);
    nEXOMCPixelatedChargeDeposit* ChargeDeposit = new nEXOMCPixelatedChargeDeposit(MyKey);
    FillPCDMap(ChargeDeposit);
    return ChargeDeposit;
  }
}

void nEXOChargeReadoutDigitize::FillPCDMap(nEXOMCPixelatedChargeDeposit* pcd) const
{
  fPCDMap[pcd->GetPixelCoordinateKey()] = pcd;
}

void nEXOChargeReadoutDigitize::FillHitMap(nEXOChargeReadout* readout) const
{
  fHitMap[readout->GetTileId()] = readout;
}

void nEXOChargeReadoutDigitize::ClearPCDs() 
{
  fPCDMap.clear();
  fInductionPCDMap.clear();
}

void nEXOChargeReadoutDigitize::GroupPCDsForInductionCalc() {
  if(fPCDZ) delete fPCDZ;
  Double_t anodeZ = nEXODigiAnalysis::GetInstance()->GetAnodeZ();
  Double_t dZ = fDriftVelocity/(CLHEP::mm/CLHEP::microsecond)*(fSamplingInterval/CLHEP::microsecond);
  fPCDZ = new TH1F("pcdZ", "", 10000, anodeZ-dZ*1500, anodeZ);

  Int_t totalNTE = 0.;
  PCDMap::iterator iter;
  for(iter= fPCDMap.begin(); iter!=fPCDMap.end(); iter++) {
    nEXOMCPixelatedChargeDeposit* pcd = iter->second;
    nEXOCoordinates pcdCenter = pcd->GetPixelCenter();

    nEXOCoordinateKey MyKey = nEXOCoordinateKey(pcdCenter.GetX(), pcdCenter.GetY(), pcdCenter.GetZ(),
                                                pcdCenter.GetT(), 1);

    nEXOMCPixelatedChargeDeposit* ChargeDeposit;
    PCDMap::iterator iterInd = fInductionPCDMap.find( MyKey );
    if ( iterInd == fInductionPCDMap.end() ) { 
      // Not find this nEXOCoordinateKey
      ChargeDeposit = new nEXOMCPixelatedChargeDeposit(MyKey);
      fInductionPCDMap[MyKey] = ChargeDeposit;
      fPCDZ->Fill(pcdCenter.GetZ());
    }
    else {
      ChargeDeposit = iterInd->second;
    }
    ChargeDeposit->fTotalTE += pcd->fTotalTE;
    totalNTE += pcd->fTotalTE;
  }
}

void nEXOChargeReadoutDigitize::ClearHitMap() 
{
  fHitMap.clear();
}

void nEXOChargeReadoutDigitize::PrintPCDMap()
{
  PCDMap::iterator iter;
  Int_t totTE = 0;
  for(iter=fPCDMap.begin(); iter!=fPCDMap.end(); iter++) {
    //nEXOCoordinateKey key = iter->first;
    nEXOMCPixelatedChargeDeposit* pcd = iter->second;
    //std::cout << "(" << key.GetCenter().GetX()
    //           << ", " << key.GetCenter().GetY()
    //           << ", " << key.GetCenter().GetZ()
    //           << "): " << pcd->fTotalTE << std::endl;
    totTE += pcd->fTotalTE;
  }
  std::cout << "fPCDMap size, total TE: "
            << fPCDMap.size() << ", "
            << totTE << std::endl;

  totTE = 0;
  for(iter=fInductionPCDMap.begin(); iter!=fInductionPCDMap.end(); iter++) {
    nEXOCoordinateKey key = iter->first;
    nEXOMCPixelatedChargeDeposit* pcd = iter->second;
    //std::cout << "(" << key.GetCenter().GetX()
    //           << ", " << key.GetCenter().GetY()
    //           << ", " << key.GetCenter().GetZ()
    //           << "): " << pcd->fTotalTE << std::endl;
    totTE += pcd->fTotalTE;
  }
  std::cout << "fInductionPCDMap size, total TE: "
            << fInductionPCDMap.size() << ", "
            << totTE << std::endl;
}

void nEXOChargeReadoutDigitize::PrintHitMap()
{
  HitMap::iterator iter;
  for(iter=fHitMap.begin(); iter!=fHitMap.end(); iter++) {
    Int_t tileId = iter->first;
    nEXOChargeReadout* readout = iter->second;
    std::cout << "-------- tile: " << tileId << "---------" << std::endl;

    for(size_t i=0; i<readout->GetNumChargeChannels(); i++) {
      nEXOChargeElecChannel* chargeChannel = readout->GetChargeChannel(i);
      std::cout << "localId: " 
        << nEXOChannelMap::GetInstance()->GetLocalId(chargeChannel->GetChannelId())
        << "charge: " << chargeChannel->GetChannelCharge()
        << "time: " << chargeChannel->GetChannelTime()
        << std::endl;
    }
  }
}

std::map<nEXOCoordinateKey, nEXOMCPixelatedChargeDeposit*>* nEXOChargeReadoutDigitize::GetPCDMap()
{
  return &fPCDMap;
}

std::map<nEXOCoordinateKey, nEXOMCPixelatedChargeDeposit*>* nEXOChargeReadoutDigitize::GetInductionPCDMap()
{
  return &fInductionPCDMap;
}

std::map<Int_t, nEXOChargeReadout*>* nEXOChargeReadoutDigitize::GetHitMap() {
  return &fHitMap;
}

//nEXOChannelMap* nEXOChargeReadoutDigitize::GetChannelMap() {
//  return fChannelMap;
//}

void nEXOChargeReadoutDigitize::CalcDriftVelocity(Double_t field) {
  // digitized table from the Liquid Xenon page:
  // http://www.pd.infn.it/~conti/images/LXe/LXe_drift_velocity.gif. 
  // NOTE that, in this plot, the velocity is ~1.89mm/us @ 380V/cm, 
  // different from the EXO-200 measured data. 
  // Such table need be updated in future.
  const Int_t Npoints = 56;
  Double_t field_velocity[Npoints*2] = {
           10, 0.1,
      10.8809, 0.234476,
      11.5433, 0.246678,
      12.9914, 0.280096,
      13.5516, 0.292206,
      15.6429, 0.331729,
      17.7547, 0.370274,
      19.9821, 0.413325,
      21.5595, 0.442259,
      22.872 , 0.461323,
      24.6775, 0.489426,
      25.7414, 0.510587,
      28.0089, 0.546295,
      29.4642, 0.569879,
      36.0817, 0.674847,
      38.6027, 0.716002,
      44.9378, 0.805892,
      50.1505, 0.862092,
      60.8978, 0.978328, 
      69.7049, 1.06436,
      79.1149, 1.13844,
      97.7052, 1.26993,
      113.74 , 1.35806,
      129.094, 1.42802,
      146.522, 1.48883,
      164.904, 1.53915,
      191.967, 1.60439,
      225.365, 1.67229,
      260.145, 1.72848,
      307.994, 1.80153,
      377.168, 1.89326,
      465.794, 1.97265,
      595.002, 2.05486,
      792.821, 2.13982,
      1021.33, 2.24794,
      1282.8 , 2.32202,
      1557.7 , 2.37877,
      1940.03, 2.45731,
      2355.79, 2.51737,
      3112.62, 2.59936,
      3811.7 , 2.68552,
      4474.86, 2.70531,
      5342.85, 2.74823,
      6013.15, 2.76934,
      7059.32, 2.78975,
      7944.96, 2.78731,
      9171.07, 2.80819,
      10766.7, 2.82888, 
      13523  , 2.84829,
      15478.7, 2.84544,
      18795.8, 2.84135,
      21880.4, 2.86247,
      30669.7, 2.83107,
      38847.9, 2.85032,
      49206.9, 2.79721,
      64468.7, 2.74441
  };
  TGraph* graph = new TGraph();
  for(Int_t i=0; i<Npoints; i++) {
    graph->SetPoint(i, field_velocity[i*2], field_velocity[i*2+1]);
  }
  fDriftVelocity = graph->Eval(field)*CLHEP::mm/CLHEP::microsecond;
}

void nEXOChargeReadoutDigitize::PrintParameters()
{
  std::cout << "fFastDiffusionSim: " << fFastDiffusionSim << std::endl;
  std::cout << "fInductionSim: " << fInductionSim << std::endl;
  std::cout << "fElectronLifetime (us): " << fElectronLifetime/CLHEP::microsecond << std::endl; // us
  std::cout << "fDiffusionCoef (cm^2/s): " << fDiffusionCoef/(CLHEP::cm*CLHEP::cm/CLHEP::second) << std::endl; // cm^2/s
  std::cout << "Diffusion Spread with 500us drift time (mm): "
            << TMath::Sqrt(fDiffusionCoef * 500*CLHEP::microsecond)/CLHEP::mm << endl;
  std::cout << "fNoiseSigma: " << fNoiseSigma << std::endl;
  std::cout << "Padsize: " << nEXOChannelMap::GetInstance()->GetPadSize()  << std::endl;
  std::cout << "fDriftVelocity (mm/us): " << fDriftVelocity/(CLHEP::mm/CLHEP::microsecond) << std::endl;
  std::cout << "fSeed: " << fSeed << std::endl;
  std::cout << "fDebug: " << fDebug << std::endl;
  std::cout << "fSamplingInterval (us): " << fSamplingInterval/CLHEP::microsecond << std::endl; // us
}
