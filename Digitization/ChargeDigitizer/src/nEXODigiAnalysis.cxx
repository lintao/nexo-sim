#include "nEXODigiAnalysis.hh"
#include "TGeoManager.h"
#include "TGeoNode.h"
#include <iostream>

#ifndef nEXODimensions_hh
#include "nEXOUtilities/nEXODimensions.hh"
#endif

#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/SniperDataPtr.h"
#include "BufferMemMgr/EvtDataPtr.h"
#include "Event/SimHeader.h"
#include "Event/ElecHeader.h"
#include "RootIOSvc/RootInputSvc.h"

nEXODigiAnalysis* nEXODigiAnalysis::fInstance = NULL;

nEXODigiAnalysis::nEXODigiAnalysis()
{
  fRootFile = NULL;
  fRootTree = NULL;

  ResetTreeVariables();
}

nEXODigiAnalysis::~nEXODigiAnalysis(){;}

void nEXODigiAnalysis::GetTE(int i, Double_t& E, Double_t& X, Double_t& Y, Double_t& Z)
{
  E = fTEEnergy[i];
  X = fTEX[i];
  Y = fTEY[i];
  Z = fTEZ[i];
}

Int_t nEXODigiAnalysis::GetNumTE() {return fNTE; }

void nEXODigiAnalysis::ResetTreeVariables(void)
{
  fEventNumber = 0;
  fTotalEventEnergy = 0;
  fNumDeposits = 0;
  fInitNOP = 0;
  fNOP = 0;
  fNTE = 0;
  fGenX = 0;
  fGenY = 0;
  fGenZ = 0;

  for (int i = 0; i < MAXOPNUM; i++) {
    fSiPMID[i] = 0; 
    fOPTime[i] = 0; 
    fOPEnergy[i] = 0; 
    fOPStopVolume[i] = 0; 
    fOPX[i] = 0; 
    fOPY[i] = 0; 
    fOPZ[i] = 0; 
  }
  for (int i = 0; i < MAXTENUM; i++)
  {
    fTEX[i] = 0; 
    fTEY[i] = 0;
    fTEZ[i] = 0;
    fTEEnergy[i] = 0; 
  }
  for (int i = 0; i< MAXDEPNUM; i++)
  {
    //fLengthDeposit[i] = 0; 
    fEnergyDeposit[i] = 0; 
    //fPreEnergyDeposit[i] = 0; 
    //fPostEnergyDeposit[i] = 0; 
    fTrackNumber[i] = 0; 
    fXpos[i] = 0; 
    fYpos[i] = 0; 
    fZpos[i] = 0;
    //fTglob[i] = 0;
    //fTloc[i] = 0;
  }
}

void nEXODigiAnalysis::SetTreeBranches(TString filename, TString treename)
{
  if(!fRootFile) fRootFile = new TFile(filename);
//   fRootTree = (TTree*) fRootFile->Get(treename);
//   fRootTree->SetBranchAddress("EventNumber",&fEventNumber,&b_EventNumber);
//   fRootTree->SetBranchAddress("GenX", &fGenX, &b_GenX);
//   fRootTree->SetBranchAddress("GenY", &fGenY, &b_GenY);
//   fRootTree->SetBranchAddress("GenZ", &fGenZ, &b_GenZ);
//   fRootTree->SetBranchAddress("TotalEventEnergy", &fTotalEventEnergy, &b_TotalEventEnergy);
//   fRootTree->SetBranchAddress("NumDeposits",&fNumDeposits, &b_NumDeposits);
//   fRootTree->SetBranchAddress("EnergyDeposit",&fEnergyDeposit, &b_EnergyDeposit);
// //  fRootTree->SetBranchAddress("TrackNumber",&fTrackNumber, &b_TrackNumber);
//   //fRootTree->SetBranchAddress("Xpos", &fXpos, &b_Xpos);
//   //fRootTree->SetBranchAddress("Ypos", &fYpos, &b_Ypos);
//   //fRootTree->SetBranchAddress("Zpos", &fZpos, &b_Zpos); 
//   fRootTree->SetBranchAddress("InitNumOP", &fInitNOP, &b_InitNOP);
// //  fRootTree->SetBranchAddress("OPStopVolume", fOPStopVolume, &b_OPStopVolume);
// //  fRootTree->SetBranchAddress("NumOP", &fNOP, &b_NumOP);
// //  fRootTree->SetBranchAddress("SiPMID", &fSiPMID, &b_SiPMID);
// //  fRootTree->SetBranchAddress("OPEnergy", &fOPEnergy, &b_OPEnergy);
// //  fRootTree->SetBranchAddress("OPTime", &fOPTime, &b_OPTime);
// //  fRootTree->SetBranchAddress("OPX", &fOPX, &b_OPX);
// //  fRootTree->SetBranchAddress("OPY", &fOPY, &b_OPY);
// //  fRootTree->SetBranchAddress("OPZ", &fOPZ, &b_OPZ);
//   fRootTree->SetBranchAddress("NumTE", &fNTE, &b_NTE);
// //  fRootTree->SetBranchAddress("TEEnergy", &fTEEnergy, &b_TEEnergy);
//   fRootTree->SetBranchAddress("TEX", &fTEX, &b_TEX);
//   fRootTree->SetBranchAddress("TEY", &fTEY, &b_TEY);
//   fRootTree->SetBranchAddress("TEZ", &fTEZ, &b_TEZ);
//

  // try to load geometry from input service
  SniperPtr<RootInputSvc> inputsvc("InputSvc");
  TGeoManager* gm = 0;
  if (inputsvc.valid() ){
    TObject* obj = 0;
    inputsvc->getObj(obj, "nEXOGeometry", "/Event/Sim");
    std::cout << "LT: obj: " << obj << std::endl;
    if (obj) {
      gm = dynamic_cast<TGeoManager*>(obj);
    }
  } else {
    std::cout << "Can't find InputSvc" << std::endl;
  }

  // Geometry info
  if (!gm) {
    gm = (TGeoManager*) fRootFile->Get("nEXOGeometry");
  }
  TGeoNode* gnd = gm->FindNode(0, 0, 0);
  gm->SetCurrentDirection(0, 0, 1);
  TString name = gnd->GetName();
  while(name!="/nEXO/TPCInternals/Anode_0") {
    gm->FindNextBoundaryAndStep();
    gnd = gm->GetCurrentNode();
    name = gnd->GetName();
  }
  Double_t* trans = gm->GetCurrentMatrix()->GetTranslation();
  cout << name << ", current Z: " << trans[2] << endl;
  fAnodeZ = trans[2]*CLHEP::cm/CLHEP::mm; // cm --> mm
}

void nEXODigiAnalysis::GetEntry(Int_t i)
{
  ResetTreeVariables();
  // fRootTree->GetEntry(i);

  // load data from data buffer
  EvtDataPtr<nEXO::SimHeader> header("/Event/Sim");
  std::cout << " debug the event data " << std::endl;
  std::cout << " header: " << header.data() << std::endl;
  if (header.invalid()) {
    std::cout << "Header is invalid. Please check the input data." << std::endl;
    return;
  }
  nEXO::SimEvent* event = dynamic_cast<nEXO::SimEvent*>(header->event());
  std::cout << " event: " << event << std::endl;
  std::cout << " gen x/y/z: (" 
            << event->GenX() << "," 
            << event->GenY() << "," 
            << event->GenZ() << ")" 
            << std::endl;

  fEventNumber = event->EventNumber();
  fGenX = event->GenX();
  fGenY = event->GenY();
  fGenZ = event->GenZ();
  fTotalEventEnergy = event->TotalEventEnergy();
  fNumDeposits = event->NumDeposits();
  for (int i = 0; i < fNumDeposits; ++i) {
    fEnergyDeposit[i] = event->EnergyDeposit()[i];
  }
  fNTE = event->NTE();
  for (int i = 0; i < fNTE; ++i) {
    fTEEnergy[i] = event->TEEnergy()[i];
    fTEX[i] = event->TEX()[i];
    fTEY[i] = event->TEY()[i];
    fTEZ[i] = event->TEZ()[i];
  }
            
  std::cout << " --------- end debug --------- " << std::endl;
}

Long64_t nEXODigiAnalysis::GetEntries()
{
  // return fRootTree->GetEntries();
  return -1;
}

void nEXODigiAnalysis::CreateOutputFile(TString OutputFileName, TString OutputTreeName)
{
  fOutFile = new TFile(OutputFileName, "recreate");
  fOutTree = new TTree(OutputTreeName, "");

  fOutTree->Branch("EventNumber",&fEventNumber,"EventNumber/I"); 
  fOutTree->Branch("Energy", &fEnergy, "Energy/D");
  fOutTree->Branch("GenX", &fGenX, "GenX/D");
  fOutTree->Branch("GenY", &fGenY, "GenY/D");
  fOutTree->Branch("GenZ", &fGenZ, "GenZ/D");
  fOutTree->Branch("InitNumOP", &fInitNOP, "InitNumOP/I");

/*  fOutTree->Branch("NumPCDs", &fNumPCDs, "NumPCDs/I");
  fOutTree->Branch("PCDx", &fPCDx, "PCDx[NumPCDs]/D");
  fOutTree->Branch("PCDy", &fPCDy, "PCDy[NumPCDs]/D");
  fOutTree->Branch("PCDz", &fPCDz, "PCDz[NumPCDs]/D");
  fOutTree->Branch("PCDe", &fPCDe, "PCDe[NumPCDs]/D");
  fOutTree->Branch("PCDq", &fPCDq, "PCDq[NumPCDs]/D");

  fOutTree->Branch("NumTiles", &fNumTiles, "NumTiles/I");
  fOutTree->Branch("NumChannelsOnTile", &fNumChannelsOnTile, "NumChannelsOnTile[NumTiles]/I");
  fOutTree->Branch("TileCharge", &fTileCharge, "TileCharge[NumTiles]/D");
*/
  fOutTree->Branch("NumChannels", &fNumChannels, "NumChannels/I");

//  fOutTree->Branch("TileId", &fTileId, "TileId[NumChannels]/I");
  fOutTree->Branch("xTile", &fxTile, "xTile[NumChannels]/D");
  fOutTree->Branch("yTile", &fyTile, "yTile[NumChannels]/D");
  fOutTree->Branch("XPosition", &fXPosition, "XPosition[NumChannels]/D");
  fOutTree->Branch("YPosition", &fYPosition, "YPosition[NumChannels]/D");
  fOutTree->Branch("ChannelLocalId", &fChannelLocalId, "ChannelLocalId[NumChannels]/I");
  fOutTree->Branch("ChannelCharge", &fChannelCharge, "ChannelCharge[NumChannels]/D");
//  fOutTree->Branch("ChannelFirstTime", &fChannelFirstTime, "ChannelFirstTime[NumChannels]/D");
//  fOutTree->Branch("ChannelLatestTime", &fChannelLatestTime, "ChannelLatestTime[NumChannels]/D");
  fOutTree->Branch("ChannelTime", &fChannelTime, "ChannelTime[NumChannels]/D");
  fOutTree->Branch("ChannelNTE", &fChannelNTE, "ChannelNTE[NumChannels]/I");
  fOutTree->Branch("ChannelNoiseTag", &fChannelNoiseTag, "ChannelNoiseTag[NumChannels]/I");
//  fOutTree->Branch("InductionAmplitude", &fInductionAmplitude, "InductionAmplitude[NumChannels]/D");

  //std::vector<Double_t> fChannelWaveform[10000];
  fOutTree->Branch("NumCC", &fNumCC, "NumCC/I");
  fOutTree->Branch("ccEnergy", &fccEnergy, "ccEnergy[NumCC]/D");
  fOutTree->Branch("ccX", &fccX, "ccX[NumCC]/D");
  fOutTree->Branch("ccY", &fccY, "ccY[NumCC]/D");
  fOutTree->Branch("ccT", &fccT, "ccT[NumCC]/D");
  fOutTree->Branch("ccZ", &fccZ, "ccZ[NumCC]/D");
  fOutTree->Branch("ccXRMS", &fccXRMS, "ccXRMS[NumCC]/D");
  fOutTree->Branch("ccYRMS", &fccYRMS, "ccYRMS[NumCC]/D");
  fOutTree->Branch("ccZRMS", &fccTRMS, "ccZRMS[NumCC]/D");
  fOutTree->Branch("ccTRMS", &fccTRMS, "ccTRMS[NumCC]/D");
  fOutTree->Branch("ccType", &fccType, "ccType[NumCC]/C");

  fOutTree->Branch("ssEnergy", &fssEnergy, "ssEnergy/D");
  fOutTree->Branch("msEnergy", &fmsEnergy, "msEnergy/D");

  // waveform tree
  fWaveformTree = new TTree("waveformTree", "");
  fWaveformTree->Branch("WFLen", &fWFLen, "WFLen/I");
  fWaveformTree->Branch("WFTileId", &fWFTileId, "WFTileId/I");
  fWaveformTree->Branch("WFLocalId", &fWFLocalId, "WFLocalId/I");
  fWaveformTree->Branch("WFChannelCharge", &fWFChannelCharge, "WFChannelCharge/D");
  fWaveformTree->Branch("WFAmplitude", &fWFAmplitude, "WFAmplitude[WFLen]/D");
  fWaveformTree->Branch("WFTime", &fWFTime, "WFTime[WFLen]/D");
}

void nEXODigiAnalysis::FillClusters(nEXOEventData* ED)
{
  fNumCC = ED->GetNumChargeClusters();
  int xcluster=0, ycluster=0;
  for(size_t i=0; i<fNumCC; i++) {
    nEXOChargeCluster* cc = ED->GetChargeCluster(i);
    fccEnergy[i] = cc->fCharge;
    fccX[i] = cc->fX;
    fccY[i] = cc->fY;
    fccT[i] = cc->fT;
    fccZ[i] = cc->fZ;
    fccXRMS[i] = cc->fXRMS;
    fccYRMS[i] = cc->fYRMS;
    fccTRMS[i] = cc->fTRMS;
    fccZRMS[i] = cc->fZRMS;
	 fccType[i] = cc->fChannelType;
	 if(cc->fChannelType=='X'){
		 xcluster++;
	 }
	 else if(cc->fChannelType=='Y'){
		 ycluster++;
	 }
  }

  if(xcluster+ycluster<=1) {
    fssEnergy = fccEnergy[0];
  }
  else if(xcluster==1&&ycluster==1&&fabs(fccT[0]-fccT[1])<1000){
	  fssEnergy = fccEnergy[0]+fccEnergy[1];
  }
  else {
    fmsEnergy = 0.;
    for(size_t i=0; i<fNumCC; i++) fmsEnergy += fccEnergy[i];
  }
}

void nEXODigiAnalysis::Fill(nEXOChargeReadoutDigitize* chargeDigi)
{
  SniperDataPtr<nEXO::NavBuffer>  navBuf("/Event");
  if (navBuf.invalid()) {
      LogError << "Can't find the buffer " << std::endl;
      return;
  }
  nEXO::EvtNavigator* evt_nav = navBuf->curEvt();
  if (!evt_nav) {
      LogError << "Can't find the event navigator " << std::endl;
      return;
  }
  // Data model related
  nEXO::ElecHeader* header = new nEXO::ElecHeader();
  evt_nav->addHeader(header); 

  nEXO::ElecEvent* elec_event = new nEXO::ElecEvent();
  header->setEvent(elec_event);

  //-----------------------PCD Map-----------------------
  std::map<nEXOCoordinateKey, nEXOMCPixelatedChargeDeposit*>::iterator pcdIter;
  std::map<nEXOCoordinateKey, nEXOMCPixelatedChargeDeposit*> *pcdMap = chargeDigi->GetPCDMap();
  fNumPCDs = pcdMap->size();

  Int_t iPCD = 0;
  for(pcdIter=pcdMap->begin(); pcdIter!=pcdMap->end(); pcdIter++) {
    nEXOMCPixelatedChargeDeposit *pcd = pcdIter->second;
    nEXOCoordinates coord = pcd->GetPixelCenter();
    fPCDx[iPCD] = coord.GetX();
    fPCDy[iPCD] = coord.GetY();
    fPCDz[iPCD] = coord.GetZ();
    fPCDe[iPCD] = pcd->fTotalEnergy;
    fPCDq[iPCD] = pcd->fTotalTE;

    iPCD++;
  }
  
  //-----------------------Hit Map-----------------------
  //chargeDigi->PrintHitMap();

  std::map<Int_t, nEXOChargeReadout*>::iterator iter;
  std::map<Int_t, nEXOChargeReadout*> *hitMap = chargeDigi->GetHitMap();
  fNumTiles = hitMap->size();

  Int_t iTile = 0, iCh = 0;
  for(iter=hitMap->begin(); iter!=hitMap->end(); iter++) {
    Int_t tileId = iter->first;
    nEXOChargeReadout* readout = iter->second;

    fNumChannelsOnTile[iTile] = readout->GetNumChargeChannels();
    fNumChannels += fNumChannelsOnTile[iTile];

    for (size_t k=0; k<fNumChannelsOnTile[iTile]; k++) {
      nEXOChargeElecChannel* elecChannel = readout->GetChargeChannel(k);
      Int_t localId = nEXOChannelMap::GetInstance()->GetLocalId(elecChannel->GetChannelId());
      fTileId[iCh] = tileId;
      fxTile[iCh] = nEXOChannelMap::GetInstance()->GetTilePosition(tileId).x();
      fyTile[iCh] = nEXOChannelMap::GetInstance()->GetTilePosition(tileId).y();;
      fXPosition[iCh] = nEXOChannelMap::GetInstance()->GetChannelPosition(tileId, localId).x();
      fYPosition[iCh] = nEXOChannelMap::GetInstance()->GetChannelPosition(tileId, localId).y();
      //if(localId>=33) std::cout << "fYPosition: " << fYPosition[iCh] << std::endl;
      //else std::cout << "fXPosition: " << fYPosition[iCh] << std::endl;
      fChannelCharge[iCh] = elecChannel->GetChannelCharge();
      fChannelFirstTime[iCh] = elecChannel->GetChannelFirstTime();
      fChannelLatestTime[iCh] = elecChannel->GetChannelLatestTime();
      fChannelTime[iCh] = elecChannel->GetChannelTime();
      fChannelLocalId[iCh] = localId;
      fChannelInductionAmplitude[iCh] = elecChannel->GetChannelInductionAmplitude();
      fTileCharge[iTile] += fChannelCharge[iCh];
      fChannelNTE[iCh] = elecChannel->GetNTE();
      fChannelNoiseTag[iCh] = elecChannel->GetNoiseTag();

      // Data model related
      nEXO::ElecChannel elec_channel;
      elec_channel.TileId                   (fTileId                   [iCh]);
      elec_channel.xTile                    (fxTile                    [iCh]);
      elec_channel.yTile                    (fyTile                    [iCh]);
      elec_channel.XPosition                (fXPosition                [iCh]);
      elec_channel.YPosition                (fYPosition                [iCh]);
      elec_channel.ChannelLocalId           (fChannelLocalId           [iCh]);
      elec_channel.ChannelCharge            (fChannelCharge            [iCh]);
      elec_channel.ChannelInductionAmplitude(fChannelInductionAmplitude[iCh]);
      elec_channel.ChannelFirstTime         (fChannelFirstTime         [iCh]);
      elec_channel.ChannelLatestTime        (fChannelLatestTime        [iCh]);
      elec_channel.ChannelTime              (fChannelTime              [iCh]);
      elec_channel.ChannelNTE               (fChannelNTE               [iCh]);
      elec_channel.ChannelNoiseTag          (fChannelNoiseTag          [iCh]);
      elec_channel.InductionAmplitude       (fInductionAmplitude       [iCh]);

      std::cout << "LT:: show channel data " << std::endl;
      std::cout << elec_channel.TileId                   () << " " << fTileId                   [iCh]<< std::endl;;
      std::cout << elec_channel.xTile                    () << " " << fxTile                    [iCh]<< std::endl;;
      std::cout << elec_channel.yTile                    () << " " << fyTile                    [iCh]<< std::endl;;
      std::cout << elec_channel.XPosition                () << " " << fXPosition                [iCh]<< std::endl;;
      std::cout << elec_channel.YPosition                () << " " << fYPosition                [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelLocalId           () << " " << fChannelLocalId           [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelCharge            () << " " << fChannelCharge            [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelInductionAmplitude() << " " << fChannelInductionAmplitude[iCh]<< std::endl;;
      std::cout << elec_channel.ChannelFirstTime         () << " " << fChannelFirstTime         [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelLatestTime        () << " " << fChannelLatestTime        [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelTime              () << " " << fChannelTime              [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelNTE               () << " " << fChannelNTE               [iCh]<< std::endl;;
      std::cout << elec_channel.ChannelNoiseTag          () << " " << fChannelNoiseTag          [iCh]<< std::endl;;
      std::cout << elec_channel.InductionAmplitude       () << " " << fInductionAmplitude       [iCh]<< std::endl;;
      std::cout << "---------------------- " << std::endl;

      iCh++;

      //std::cout << "nEXOChargeReadoutDigitize::GetInstance()->GetSaveWaveform(): "
      //          << nEXOChargeReadoutDigitize::GetInstance()->GetSaveWaveform() << std::endl;
      if(nEXOChargeReadoutDigitize::GetInstance()->GetSaveWaveform()==true){
        fWFLen = elecChannel->GetWaveformLength();
        fWFLocalId = localId;
        fWFTileId = tileId;
        fWFChannelCharge = elecChannel->GetChannelCharge();

        elec_channel.WFLen(fWFLen);
        elec_channel.WFTileId(fWFTileId);
        elec_channel.WFLocalId(fWFLocalId);
        elec_channel.WFChannelCharge(fWFChannelCharge);

        std::vector<Double_t> tmp_amp;
        std::vector<Double_t> tmp_time;

        for (UInt_t iWF=0; iWF<fWFLen; iWF++) {
          fWFAmplitude[iWF] = elecChannel->GetWaveformAmplitude(iWF);
          fWFTime[iWF] = elecChannel->GetWaveformTime(iWF);

          tmp_amp.push_back(fWFAmplitude[iWF]);
          tmp_time.push_back(fWFTime[iWF]);
        }

        elec_channel.WFAmplitude(tmp_amp);
        elec_channel.WFTime(tmp_time);
        //std::cout << "fWFLen, fWFLocalId, fWFTileId: " << fWFLen << ", "
        //          << fWFLocalId << ", " << fWFTileId << std::endl;
        fWaveformTree->Fill();
      }

      // Data model related
      elec_event->addElecChannel(elec_channel);
    }

    iTile++;
  }
  fEnergy = fTotalEventEnergy;
}

void nEXODigiAnalysis::FillOutputTree()
{
  fOutTree->Fill();
  ResetOutputTree();
}

void nEXODigiAnalysis::ResetOutputTree()
{
  fNumPCDs = 0;
  for( Int_t i=0; i<MAXPCDNUM; i++) {
    fPCDx[i] = 0.;
    fPCDy[i] = 0.;
    fPCDz[i] = 0.;
    fPCDe[i] = 0.;
    fPCDq[i] = 0.;
  }

  fNumTiles = 0;
  for( Int_t i=0; i<200; i++) {
    fNumChannelsOnTile[i] = 0;
    fTileCharge[i] = 0.;
  }
  fNumChannels = 0;
  for( Int_t i=0; i<MAXPCDNUM; i++) {
    fTileId[i] = 0;
    fxTile[i] = 0;
    fyTile[i] = 0;
    fXPosition[i] = 0;
    fYPosition[i] = 0;
    fChannelCharge[i] = 0.;
    fChannelFirstTime[i] = 0.;
    fChannelLatestTime[i] = 0.;
    fChannelTime[i] = 0.;
    fChannelLocalId[i] = 0; 
    fInductionAmplitude[i] = 0.;
  }

  for( Int_t i=0; i<1000; i++) {
    fWFAmplitude[i] = 0;
    fWFTime[i] = 0;
  }

  fNumCC = 0;
  fssEnergy = 0.;
  fmsEnergy = 0.;
  for( Int_t i=0; i<100; i++) {
    fccEnergy[i] = 0.;
    fccX[i] = 0.;
    fccY[i] = 0.;
    fccT[i] = 0.;
    fccXRMS[i] = 0.;
    fccYRMS[i] = 0.;
    fccTRMS[i] = 0.;
	  fccType[i] = 'N';
  }
}

void nEXODigiAnalysis::Write()
{
  fOutFile->Write();
  fOutFile->Close();
}
