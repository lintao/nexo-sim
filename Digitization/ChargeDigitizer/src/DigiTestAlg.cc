#include "DigiTestAlg.h"
#include "SniperKernel/AlgFactory.h"
#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/SniperDataPtr.h"
#include "BufferMemMgr/EvtDataPtr.h"
#include "Event/SimHeader.h"
#include "Event/ElecHeader.h"

DECLARE_ALGORITHM(DigiTestAlg);

DigiTestAlg::DigiTestAlg(const std::string& name)
    : AlgBase(name)
{

}

DigiTestAlg::~DigiTestAlg()
{

}

bool
DigiTestAlg::initialize()
{
    return true;
}

bool
DigiTestAlg::execute()
{
    EvtDataPtr<nEXO::ElecHeader> header("/Event/Elec");
    if (header.invalid()) {
        LogError << "can't find the header." << std::endl;
        return false;
    }
    nEXO::ElecEvent* event = dynamic_cast<nEXO::ElecEvent*>(header->event());

    // get the channels
    const std::vector<nEXO::ElecChannel>& channels = event->ElecChannels();

    for (std::vector<nEXO::ElecChannel>::const_iterator it = channels.begin();
            it != channels.end(); ++it) {
        const nEXO::ElecChannel& channel = (*it);
        // get the waveform
        const std::vector<Double_t>& ampl = channel.WFAmplitude();
        const std::vector<Double_t>& time = channel.WFTime();
        std::cout << "ampl/time size: " 
                  << ampl.size() 
                  << "/"
                  << time.size() 
                  << std::endl;
    }
    return true;
}

bool
DigiTestAlg::finalize()
{
    return true;
}
