#ifndef ChargeDigitizerAlg_h
#define ChargeDigitizerAlg_h

#include "SniperKernel/AlgBase.h"
#include "TROOT.h"
#include "TString.h"
#include "nEXODigiAnalysis.hh"
#include "nEXOChargeReadoutDigitize.hh"
#include "nEXOClustering.hh"

#ifndef nEXOEventData_hh
#include "nEXOUtilities/nEXOEventData.hh"
#endif

class ChargeDigitizerAlg: public AlgBase
{
public:
    ChargeDigitizerAlg(const std::string& name);
    ~ChargeDigitizerAlg();

    bool initialize();
    bool execute();
    bool finalize();
private:
    std::string InputFileName;
    std::string InputTreeName;
    std::string tileMapName;
    std::string padsMapName;
    std::string coType;
    std::string wpFileName;

    std::string OutputFileName;
    std::string OutputTreeName;

    double ElectronLifeT;
    double DiffusionCoef;
    double NoiseCoef;
    double PadSize;
    double SamplingInteval;
    bool InductionSim;
    double CalcDriftVelocity;
    bool SaveWaveform;

    nEXOChargeReadoutDigitize* chargeDigi;
    nEXOClustering* clusterTool;

    Long64_t evtid;
    Long64_t nEvents;
};

#endif
