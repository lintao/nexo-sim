
#include "ChargeDigitizerAlg.h"
#include "SniperKernel/AlgFactory.h"
#include "SniperKernel/Incident.h"
#include<iostream>
#include <cstdlib>

DECLARE_ALGORITHM(ChargeDigitizerAlg);

ChargeDigitizerAlg::ChargeDigitizerAlg(const std::string& name)
    : AlgBase(name), chargeDigi(0), clusterTool(0), evtid(-1)
{
    declProp("InputFileName", InputFileName="detsim.root");
    declProp("InputTreeName", InputTreeName="nEXOevents");

    declProp("tileMapName"    , tileMapName);
    declProp("padsMapName"    , padsMapName);
    declProp("coType"         , coType="Pads");
    declProp("wpFileName"     , wpFileName);
    declProp("OutputFileName" , OutputFileName="digi.root");
    declProp("OutputTreeName" , OutputTreeName="evtTree");

    declProp("ElectronLifeT", ElectronLifeT=10.0e3); // microsecond
    declProp("DiffusionCoef", DiffusionCoef=0.); // cm^2/s
    declProp("NoiseCoef",     NoiseCoef=0.); //
    declProp("PadSize",       PadSize=3.); // mm
    declProp("SamplingInteval", SamplingInteval=0.5); // microsecond

    declProp("InductionSim", InductionSim=true); // true or false

    declProp("CalcDriftVelocity", CalcDriftVelocity=250.); // V/cm

    declProp("SaveWaveform", SaveWaveform=true); // true or false

}

ChargeDigitizerAlg::~ChargeDigitizerAlg()
{

}

bool
ChargeDigitizerAlg::initialize()
{
    
    if(getenv("NEXOANALYSIS")) {
        tileMapName = TString(getenv("NEXOANALYSIS")) + "/utilities/scripts/tilesMap.txt";
        padsMapName = TString(getenv("NEXOANALYSIS")) + "/utilities/scripts/localChannelsMap_3mm.txt";
        wpFileName = TString(getenv("NEXOANALYSIS")) + "/utilities/data/singlePadWP3mm.root";
    }
    // TODO
    // The chargeDigi should have its own properties
    chargeDigi = nEXOChargeReadoutDigitize::GetInstance();
    // initialize the variables
    chargeDigi->SetElectronLifetime( ElectronLifeT*CLHEP::microsecond );
    chargeDigi->SetDiffusionCoef( DiffusionCoef*(CLHEP::cm*CLHEP::cm)/CLHEP::second );
    chargeDigi->SetNoiseCoef( NoiseCoef );

    nEXOChannelMap::GetInstance()->SetPadSize( PadSize*CLHEP::mm );

    chargeDigi->SetSamplingInterval( SamplingInteval*CLHEP::microsecond);
    chargeDigi->SetInductionSim(InductionSim);
    chargeDigi->CalcDriftVelocity( CalcDriftVelocity );
    chargeDigi->SetSaveWaveform( SaveWaveform );

    // 
    if(nEXOChannelMap::GetInstance()->LoadChannelMap(tileMapName, padsMapName)==false) {
        LogError << "Failed to load channel map. " << std::endl;
        return false;
    }
    if(nEXOFieldWP::GetInstance()->LoadWP(coType, wpFileName)==false) {
        LogError << "Failed to load wp. " << std::endl;
        return false;
    }

    nEXODigiAnalysis::GetInstance()->SetTreeBranches(InputFileName, InputTreeName);
    nEXODigiAnalysis::GetInstance()->CreateOutputFile(OutputFileName, OutputTreeName);

    // nEvents = nEXODigiAnalysis::GetInstance()->GetEntries();

    chargeDigi->PrintParameters();

    clusterTool = new nEXOClustering();

    return true;
}

bool
ChargeDigitizerAlg::execute()
{
    ++evtid;
    // if (evtid >= nEvents) {
    //     return Incident::fire("StopRun");
    // }
    nEXOEventData* ED = new nEXOEventData();
    chargeDigi->GeneratePCDs(evtid, ED);
    chargeDigi->Digitize(ED);
    nEXODigiAnalysis::GetInstance()->Fill(chargeDigi);

    // clustering test
    //clusterTool->CreateChargeClusters(ED);
    //nEXODigiAnalysis::GetInstance()->FillClusters(ED);
    //clusterTool->CreateScintillationClusters(ED);
    //clusterTool->Clear(ED);
    nEXODigiAnalysis::GetInstance()->FillOutputTree();
    if(ED!=NULL) delete ED;
    return true;
}

bool
ChargeDigitizerAlg::finalize()
{
    nEXODigiAnalysis::GetInstance()->Write();
    return true;
}
