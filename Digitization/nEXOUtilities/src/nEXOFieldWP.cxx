#ifndef nEXOFieldWP_hh
#include "nEXOUtilities/nEXOFieldWP.hh"
#endif

#ifndef nEXOChannelMap_hh
#include "nEXOUtilities/nEXOChannelMap.hh"
#endif

#ifdef nEXODimensions_hh
#include "nEXOUtilities/nEXODimensions.hh"
#endif

#include "TString.h"
#include "TMath.h"
#include <fstream>
#include <iostream>
using namespace std;
nEXOFieldWP* nEXOFieldWP::fInstance = NULL;

nEXOFieldWP::nEXOFieldWP() 
{
  fWPFile = 0;
  fPadWP = 0;
  lazy_loading = true;
  fHists.clear();
}

nEXOFieldWP::~nEXOFieldWP() 
{
}

// Functions
void nEXOFieldWP::Clear(Option_t* opt)
{
  if(fWPFile!=NULL) fWPFile->Close();
  if(fSingleWPFile!=NULL) fSingleWPFile->Close();
}

bool nEXOFieldWP::LoadWP(TString coType, TString filename) 
{
  std::cout << "====== Load 3D WP field map: " << coType << ", " << filename << std::endl;
  if(coType=="Pads") {
    // Pads charge readout design
    //fWPFile = new TFile(filename, "read");
    //fPadWP = (TH3F*) fWPFile->Get("q3d");
    //
    //if(fWPFile==NULL or fPadWP==NULL) return kFALSE;
    return LoadSinglePadWP(filename);
  }
  else {
    // other options
    return kFALSE;
  }
}

bool nEXOFieldWP::LoadSinglePadWP(TString filename) {
  fSingleWPFile = new TFile(filename, "read");
  if(!fSingleWPFile) return false;

  std::cout << "====== Load Single Pad WP data: " << std::endl;

  if(fSingleWPFile->Get("xaxis")) {
    fBinsX = dynamic_cast<TAxis*>(fSingleWPFile->Get("xaxis"));
    fXBinNum = fBinsX->GetNbins();
    std::cout << "===== Load xaxis (" << fXBinNum << ") " << std::endl;
  }
  else {
    std::cout << "No xaxis!" << std::endl;
    return false;
  }

  if(fSingleWPFile->Get("yaxis")) {
    fBinsY = dynamic_cast<TAxis*>(fSingleWPFile->Get("yaxis"));
    fYBinNum = fBinsY->GetNbins();
    std::cout << "===== Load yaxis (" << fYBinNum << ") " << std::endl;
  }
  else {
    std::cout << "No yaxis!" << std::endl;
    return false;
  }

  // lazy load
  for(int i=0; i<fXBinNum; i++) {
    for(int j=0; j<fYBinNum; j++) {
      TH1F* h = 0;
      if(!lazy_loading) {
        TString histname = Form("wp_x%d_y%d", i+1, j+1);
        h = dynamic_cast<TH1F*>(fSingleWPFile->Get(histname));
      }
      fHists.push_back(h);
    }
  }
  std::cout << "===== Load hists " << fHists.size() << std::endl;
  return true;
}

TH1F* nEXOFieldWP::GetHist(Int_t binx, Int_t biny) {
  // binx: [1, fXBinNum]
  // biny: [1, fYBinNum]
  if(binx<1) { binx = 1; }
  else if (binx > fXBinNum) { binx = fXBinNum; }

  if(biny<1) { biny = 1; }
  else if (biny > fYBinNum) { biny = fYBinNum; }

  Int_t idx = (binx-1)*fYBinNum + (biny-1);

  TH1F* h = fHists[idx];
  if(!h) {
    TString histname = Form("wp_x%d_y%d", binx, biny);
    h = dynamic_cast<TH1F*>(fSingleWPFile->Get(histname));
    fHists[idx] = h;
  }
  return h;
}

Int_t nEXOFieldWP::GetXBin(Double_t x) {
  return fBinsX->FindBin(x);
}

Int_t nEXOFieldWP::GetYBin(Double_t y) {
  return fBinsY->FindBin(y);
}

Double_t nEXOFieldWP::CalcInducedChargeOnPads(Int_t chanId, Double_t xq, Double_t yq, Double_t zq)
{
  std::vector<TVector3>* padsCoord = nEXOChannelMap::GetInstance()->GetPadsCoord(chanId);

  Double_t lsumQ = 0., rsumQ = 0., sumQ = 0.;
  Int_t lbin = 0, rbin = 0;
  Double_t lZ = 0., rZ = 0.;
  std::vector<TVector3>::iterator iter;

  for(iter=(*padsCoord).begin(); iter!=(*padsCoord).end(); iter++) {
    Double_t xpad = (*iter).x();
    Double_t ypad = (*iter).y();

    // there is an rotation on the axes
    // due to the coordinates definition and orientation of Pads
    Double_t dX = TMath::Abs( ((xpad - xq) + (ypad - yq))/TMath::Sqrt(2) );
    Double_t dY = TMath::Abs( ((ypad - yq) - (xpad - xq))/TMath::Sqrt(2) );
	
    if( TMath::Sqrt(dX*dX+dY*dY)>5*nEXOChannelMap::GetInstance()->GetPadSize()) continue;

    if(TMath::Abs(dX)>2*nEXOChannelMap::GetInstance()->GetPadSize() && TMath::Abs(dY)>2*nEXOChannelMap::GetInstance()->GetPadSize()) {
      Double_t Rsq = dX*dX + dY*dY + zq*zq;
      Double_t indQ = zq / TMath::Power(Rsq, 1.5) / 2/TMath::Pi();
      indQ = indQ * (nEXOChannelMap::GetInstance()->GetPadSize()*nEXOChannelMap::GetInstance()->GetPadSize()/2);

      sumQ += indQ;
      continue;
    }

    Int_t xbin = fPadWP->GetXaxis()->FindBin(dX);
    Int_t ybin = fPadWP->GetYaxis()->FindBin(dY);
    Int_t zbin = fPadWP->GetZaxis()->FindBin(zq);

    sumQ += fPadWP->GetBinContent(fPadWP->GetBin( xbin, ybin, zbin))/1.e5;

    //if( zq > fPadWP->GetZaxis()->GetBinCenter(zbin) ) {
    //    lbin = fPadWP->GetBin( xbin, ybin, zbin);
    //    rbin = fPadWP->GetBin( xbin, ybin, zbin+1);
    //    lZ = fPadWP->GetZaxis()->GetBinCenter(zbin);
    //    rZ = fPadWP->GetZaxis()->GetBinCenter(zbin+1);
    //} 
    //else {
    //    lbin = fPadWP->GetBin( xbin, ybin, zbin-1);
    //    rbin = fPadWP->GetBin( xbin, ybin, zbin);
    //    lZ = fPadWP->GetZaxis()->GetBinCenter(zbin-1);
    //    rZ = fPadWP->GetZaxis()->GetBinCenter(zbin);
    //}

    //lsumQ += fPadWP->GetBinContent( lbin );
    //rsumQ += fPadWP->GetBinContent( rbin );
  }
  //if(lsumQ>0 && rsumQ>0)  sumQ += ( (rsumQ-lsumQ)/(rZ - lZ) *(zq - lZ) + lsumQ )/1.e5;
                    // this 1.e5 factor is the normalization factor that 
                    //used in calculating the fPadWP histogram
  return sumQ; 
}
