#ifndef nEXOChannelMap_hh
#include "nEXOUtilities/nEXOChannelMap.hh"
#endif

#ifdef nEXODimensions_hh
#include "nEXOUtilities/nEXODimensions.hh"
#endif

#include "TString.h"
#include "TRandom.h"
#include <fstream>
#include <iostream>
using namespace std;
nEXOChannelMap* nEXOChannelMap::fInstance = NULL;


nEXOChannelMap::nEXOChannelMap() 
{
  Clear();
  PAD_PITCH = 3.0; // mm, default value
}

nEXOChannelMap::~nEXOChannelMap() 
{
}

// Functions
void nEXOChannelMap::Clear(Option_t* opt) {
  fLocalChannels.clear();
  fLocalChannelsType.clear();
  fPadsCoord.clear();
  fTiles.clear();
  fTileMap.clear();
}

bool nEXOChannelMap::LoadChannelMap(TString tileFile, TString padsFile)
{
  if(ReadInPadsMap(padsFile)) std::cout << "Successfully";
  else { std::cout << "Error"; return false; }
  std::cout << " loading local pads map in each readout tile: " << padsFile << std::endl;

  if(ReadInTileMap(tileFile)) std::cout << "Successfully";
  else { std::cout << "Error"; return false; }
  std::cout << " loading readout tiles map: " << tileFile << std::endl;

  BuildPadsCoordMap();

  return true;
}

bool nEXOChannelMap::ReadInTileMap(TString filename)
{
  // for the time being, read in the channel map from external .txt file
  std::ifstream indat;
  indat.open(filename);

  if(indat.fail()) return false;

  Int_t tileNum=0, tileId=0;
  Double_t X=0., Y=0., Z=0.;

  indat >> tileId >> X >> Y >> Z;
  while(indat.eof()!=true) {
    std::cout << Form("Tile: %4d %9.4f %9.4f %9.4f", tileId, X, Y, Z) << std::endl;
    TVector3 pos = TVector3(X, Y, Z);
    fTiles[tileId] = pos;

    std::pair<Int_t, Int_t> idpair;
    if(X>0) idpair.first = (Int_t) (X/TILE_SIZE) + 1;
    else idpair.first = (Int_t) (X/TILE_SIZE) - 1;

    if(Y>0) idpair.second = (Int_t) (Y/TILE_SIZE) + 1;
    else idpair.second = (Int_t) (Y/TILE_SIZE) - 1;
    fTileMap[idpair] = tileId;

    tileNum++;
    indat >> tileId >> X >> Y >> Z;
  }
  std::cout << Form("Total %4d tiles", tileNum) << std::endl;
  indat.close();

  TILE_NUM = tileNum;

  return true;
}

bool nEXOChannelMap::ReadInPadsMap(TString filename)
{
  // for the time being, read in the channel map from external .txt file
  std::ifstream indat;
  indat.open(filename);

  if(indat.fail()) return false;

  Int_t chanNum=0, chanId=0;
  Double_t X=0., Y=0., Z=0.;
  TString chanType;

  indat >> TILE_SIZE;
  TILE_SIZE = TILE_SIZE*CLHEP::mm;
  cout << "TILE_SIZE: " << TILE_SIZE << endl;

  indat >> chanId >> X >> Y >> Z >> chanType;
  while(indat.eof()!=true) {
    std::cout << Form("Local channel: %4d %9.4f %9.4f %9.4f  %s", chanId, X, Y, Z, chanType.Data()) << std::endl;
    TVector3 pos = TVector3(X, Y, Z);
    fLocalChannels[chanId] = pos;
    fLocalChannelsType[chanId] = chanType;
    chanNum++;
    indat >> chanId >> X >> Y >> Z >> chanType;
  }
  std::cout << Form("Total %4d local channels on each tile", chanNum) << std::endl;
  indat.close();

  return true;
}

Bool_t nEXOChannelMap::FindHitChargeChannel(const nEXOCoordinates& coord, Int_t& tileId, Int_t& localId)
{
  // find the channel that be hit by this PCD with position of coord
  // If NO hit channel could be find, returen value is false.
  // Loop the tiles, and find out which tile this PCD will hit
  std::map<Int_t, TVector3>::iterator iter;
  std::vector<Int_t> atTileEdge;
  atTileEdge.clear();
  Bool_t isFindHitInsideTile = false;
  TVector3 tilePos;
  Double_t epsilon = 1.e-9;
  // find the tile that be hit
  Int_t iTx=0, iTy=0;
  if(coord.GetX()>=0.) iTx = (Int_t) ((coord.GetX()+epsilon)/TILE_SIZE) + 1;
  else iTx = (Int_t) ((coord.GetX()+epsilon)/TILE_SIZE) - 1;

  if(coord.GetY()>=0.) iTy = (Int_t) ((coord.GetY()+epsilon)/TILE_SIZE) + 1;
  else iTy = (Int_t) ((coord.GetY()+epsilon)/TILE_SIZE) - 1;

  std::pair<Int_t, Int_t> idpair(iTx, iTy);
  tileId = fTileMap[idpair];
  tilePos = fTiles[tileId];

  // find the channel that be hit
  Double_t pX = coord.GetX()-tilePos.x()+TILE_SIZE/2.;
  Double_t pY = coord.GetY()-tilePos.y()+TILE_SIZE/2.;
  Int_t xLow = (Int_t) (pX/nEXOChannelMap::GetInstance()->GetPadSize());
  Int_t yLow = (Int_t) (pY/nEXOChannelMap::GetInstance()->GetPadSize());
  Int_t xUp = xLow+1;
  Int_t yUp = yLow+1;

  Double_t dXl = TMath::Abs(pX - xLow*nEXOChannelMap::GetInstance()->GetPadSize());
  Double_t dXr = TMath::Abs(pX - xUp*nEXOChannelMap::GetInstance()->GetPadSize());
  Double_t dYl = TMath::Abs(pY - yLow*nEXOChannelMap::GetInstance()->GetPadSize());
  Double_t dYr = TMath::Abs(pY - yUp*nEXOChannelMap::GetInstance()->GetPadSize());

  if( TMath::Abs(dXl-dYl)<epsilon || TMath::Abs(dXl-dYr)<epsilon ||
      TMath::Abs(dXr-dYl)<epsilon || TMath::Abs(dXr-dYr)<epsilon) {
    std::cout << "at Channnel Edge!" << std::endl;
  }

  Double_t dist=1e6;
  if(dist>dXl) { dist=dXl; localId = xLow; }
  if(dist>dXr) { dist=dXr; localId = xUp; }
  if(dist>dYl) { dist=dYl; localId = yLow + nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2; }
  if(dist>dYr) { dist=dYr; localId = yUp + nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2; }

  // In real tile geometry, there are 'half' channels at the edges of a tile,
  // Temporary solution: if charge is at the rightest and uppest channel of a tile,
  // consider this channel is belong to the nearby tile.
  // 
  if(dist==dXr && xUp==nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2) {
    if(idpair.first==-1) idpair.first = 1;
    else idpair.first++;
    tileId = fTileMap[idpair];
    localId = 0;
  }
  else if(dist==dYr && yUp==nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2) {
    if(idpair.second==-1) idpair.second = 1;
    else idpair.second++;
    tileId = fTileMap[idpair];
    localId = nEXOChannelMap::GetInstance()->GetChannelNumOnTile()/2;
  }

  return true;
}

TVector3 nEXOChannelMap::GetChannelPosition(Int_t tileId, Int_t localId) {
  TVector3 tilePos = GetTilePosition(tileId);
  TVector3 localChannelPos = GetChannelLocalPosition(localId);
  TVector3 chanPos = tilePos + localChannelPos;
  return chanPos;
}

TVector3 nEXOChannelMap::GetChannelPosition(Int_t chanId) {
  return GetChannelPosition(GetTileId(chanId), GetLocalId(chanId));
}

TVector3 nEXOChannelMap::GetChannelLocalPosition(Int_t i) {
  return fLocalChannels[i];
}

TString nEXOChannelMap::GetChannelType(Int_t chanId) {
  return fLocalChannelsType[chanId];
}

TVector3 nEXOChannelMap::GetTilePosition(Int_t i) {
  return fTiles[i];
}

Int_t nEXOChannelMap::GetTileId(Int_t chanId) {
  return ((chanId & 0x0000ff00) >> 8);
}

Int_t nEXOChannelMap::GetLocalId(Int_t chanId) {
  return (chanId & 0x000000ff);
}

Int_t nEXOChannelMap::GetChannelId(Int_t tileId, Int_t localId) {
  // the format of the channel Id, a 32-bit number
  // 00000000 | 00000000 | tileId | localId 
  Int_t chanId = 0;
  chanId = ( (0x000000ff & tileId) << 8);
  chanId = ( (0x000000ff & localId) | chanId );
  return chanId;
}

std::vector<TVector3>* nEXOChannelMap::GetPadsCoord(Int_t chanId) {
  return fPadsCoord[chanId];
}

void nEXOChannelMap::SetPadSize(double padsize) {
  PAD_PITCH = padsize;

  // temporary solution
  Int_t N = (Int_t(PAD_PITCH/3*2))*2;
  CHARGE_PIXEL_SIZE_XY = PAD_PITCH/N/sqrt(2.);
  INDUCTION_PIXEL_SIZE_XY = PAD_PITCH/N/sqrt(2.);
}

void nEXOChannelMap::BuildPadsCoordMap() {
  std::map<Int_t, TVector3>::iterator itTile;
  std::map<Int_t, TVector3>::iterator itCh;

  for(itTile = fTiles.begin(); itTile!=fTiles.end(); itTile++){
    Int_t tileId = itTile->first;

    for(itCh = fLocalChannels.begin(); itCh!=fLocalChannels.end(); itCh++){
      Int_t localId = itCh->first;

      Int_t chanId = GetChannelId(tileId, localId);
      TVector3 chanPos = GetChannelPosition(chanId);

      fPadsCoord[chanId] = new std::vector<TVector3>();

      Int_t halfNum = GetChannelNumOnTile()/2/2;

      if(localId<GetChannelNumOnTile()/2) {
        // it's X- channel
        for(Int_t k=-halfNum; k<=halfNum; k++) {
          if(k<0) fPadsCoord[chanId]->push_back(TVector3(chanPos.x(), chanPos.y()+(k+0.5)*GetPadSize(), chanPos.z()));
          if(k>0) fPadsCoord[chanId]->push_back(TVector3(chanPos.x(), chanPos.y()+(k-0.5)*GetPadSize(), chanPos.z()));
        }
      }
      else {
        // it's Y- channel
        for(Int_t k=-halfNum; k<=halfNum; k++) {
          if(k<0) fPadsCoord[chanId]->push_back(TVector3(chanPos.x()+(k+0.5)*GetPadSize(), chanPos.y(), chanPos.z()));
          if(k>0) fPadsCoord[chanId]->push_back(TVector3(chanPos.x()+(k-0.5)*GetPadSize(), chanPos.y(), chanPos.z()));
        }
      }
    }
  }
}
