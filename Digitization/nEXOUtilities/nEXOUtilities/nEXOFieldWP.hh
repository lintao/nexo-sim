#ifndef nEXOFieldWP_hh
#define nEXOFieldWP_hh 1

#ifndef nEXOCoordinates_hh 
#include "nEXOUtilities/nEXOCoordinates.hh"
#endif

#ifndef nEXODimensions_hh
#include "nEXOUtilities/nEXODimensions.hh"
#endif

#include <map>
#include <vector>
#include <utility>
#include "TString.h"
#include "TVector3.h"
#include "TH1F.h"
#include "TH3F.h"
#include "TFile.h"

class nEXOFieldWP
{
  public:

    static nEXOFieldWP* GetInstance() {
      if(nEXOFieldWP::fInstance == NULL) nEXOFieldWP::fInstance = new nEXOFieldWP();
      return nEXOFieldWP::fInstance;
    }

    // Functions
    void Clear(Option_t* opt = "");
    bool LoadWP(TString coType, TString filename);
    bool LoadSinglePadWP(TString filename);
    TH1F* GetHist(Int_t binx, Int_t biny);
    Int_t GetXBin(Double_t x);
    Int_t GetYBin(Double_t y);
    Double_t  CalcInducedChargeOnPads(Int_t chanId, Double_t xq, Double_t yq, Double_t zq);
    void SetLazyLoad(bool isLazy) { lazy_loading = isLazy; }

    TString fChargeReadoutType;
    TFile*  fWPFile;
    TFile*  fSingleWPFile;

    bool lazy_loading;
    std::vector<TH1F*> fHists;

  protected:
    TH3F* fPadWP;
    TAxis* fBinsX;
    TAxis* fBinsY;
    Int_t fXBinNum;
    Int_t fYBinNum;

  private:
    nEXOFieldWP();
    ~nEXOFieldWP();
    static nEXOFieldWP* fInstance;
};
#endif
