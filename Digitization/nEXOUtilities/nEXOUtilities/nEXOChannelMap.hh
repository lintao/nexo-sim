#ifndef nEXOChannelMap_hh
#define nEXOChannelMap_hh 1

#ifndef nEXOCoordinates_hh 
#include "nEXOUtilities/nEXOCoordinates.hh"
#endif

#ifndef nEXODimensions_hh
#include "nEXOUtilities/nEXODimensions.hh"
#endif

#include <map>
#include <vector>
#include "TString.h"
#include "TVector3.h"

class nEXOChannelMap
{

  public:

    static nEXOChannelMap* GetInstance() {
      if(nEXOChannelMap::fInstance == NULL) nEXOChannelMap::fInstance = new nEXOChannelMap();
      return nEXOChannelMap::fInstance;

    }

    // Functions
    void Clear(Option_t* opt = "");
    bool ReadInTileMap(TString filename);
    bool ReadInPadsMap(TString filename);
    bool LoadChannelMap(TString tileFile, TString padsFile);

    Bool_t FindHitChargeChannel(const nEXOCoordinates& coord, Int_t& tileId, Int_t& localId);
    TVector3 GetChannelLocalPosition(Int_t i);
    TVector3 GetTilePosition(Int_t i);

    TVector3 GetChannelPosition(Int_t tileId, Int_t localId);
    TVector3 GetChannelPosition(Int_t chanId); // global channel Id
    TString  GetChannelType(Int_t chanId);
	 int ChannelNumOnTile;
	 void SetPadSize(double padsize);
   void SetTileNum(int tileNum) { TILE_NUM = tileNum; }
	 double GetPadSize(){return PAD_PITCH;};
	 int GetChannelNumOnTile(){return int(TILE_SIZE/PAD_PITCH)*2;};

	 double GetMCCHARGE_PIXEL_SIZE_XY(){return CHARGE_PIXEL_SIZE_XY; };
	 double GetMCINDUCTION_PIXEL_SIZE_XY() {return INDUCTION_PIXEL_SIZE_XY; };
   int    GetTileNum() { return TILE_NUM; }

    // extract the tile Id and local Id from the global channel Id
    Int_t GetTileId(Int_t chanId);
    Int_t GetLocalId(Int_t chanId);

    // convert (tileId, localId) pair into a global channel Id
    Int_t GetChannelId(Int_t tileId, Int_t localId);

    std::vector<TVector3>* GetPadsCoord(Int_t chanId);
    void BuildPadsCoordMap();

	 double PAD_PITCH;
   double CHARGE_PIXEL_SIZE_XY;
   double INDUCTION_PIXEL_SIZE_XY;
   int    TILE_NUM;
   double TILE_SIZE;

  protected:

    std::map<Int_t, TVector3> fTiles; // map: tile Id, tile position
    std::map<Int_t, TVector3> fLocalChannels; // map: local channel Id, local channel position
    std::map<Int_t, std::vector<TVector3>* > fPadsCoord; // map: global channel Id, global pads' positions in channel
    std::map<Int_t, TString> fLocalChannelsType;
    std::map<std::pair<Int_t, Int_t>, Int_t> fTileMap;

  private:
    nEXOChannelMap();
    ~nEXOChannelMap();
    static nEXOChannelMap* fInstance;
};
#endif
