#ifndef DetSim0Svc_hh
#define DetSim0Svc_hh

#include <SniperKernel/SvcBase.h>
#include <DetSimAlg/IDetSimFactory.h>
#include <vector>
#include <string>

class DetSim0Svc: public SvcBase, public IDetSimFactory {
public:
    DetSim0Svc(const std::string& name);
    ~DetSim0Svc();

    bool initialize();
    bool finalize();

    G4VUserDetectorConstruction* createDetectorConstruction();
    G4VUserPhysicsList* createPhysicsList();
    G4VUserPrimaryGeneratorAction* createPrimaryGenerator();

    G4UserRunAction*      createRunAction(); 
    G4UserEventAction*    createEventAction(); 
    G4UserStackingAction* createStackingAction();
    G4UserTrackingAction* createTrackingAction();
    G4UserSteppingAction* createSteppingAction();
private:
    std::vector<std::string> m_ana_list;
    std::string m_physics_lists_name;
};

#endif
