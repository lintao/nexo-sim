#include "DetSim0Svc.hh"
#include "SniperKernel/SvcFactory.h"
#include "SniperKernel/ToolBase.h"
#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/SniperLog.h"

#include "Randomize.hh"
#include "DetSimAlg/MgrOfAnaElem.h"
#include "DetSimAlg/DetSimAlg.h"
#include "DetSimAlg/IAnalysisElement.h"

#include "G4PhysListFactory.hh"


#include "nEXODetectorConstruction.hh"
#include "nEXOPrimaryGeneratorAction.hh"
#include "nEXORunAction.hh"
#include "nEXOEventAction.hh"
#include "nEXOStackingAction.hh"
#include "nEXOTrackingAction.hh"
#include "nEXOSteppingAction.hh"
#ifdef G4_V10
#include "nEXOPhysicsList.hh"
#endif

#include "EXOPhysicsList.hh"

DECLARE_SERVICE(DetSim0Svc);

DetSim0Svc::DetSim0Svc(const std::string& name)
    : SvcBase(name)
{
    declProp("AnaMgrList", m_ana_list);
    declProp("PhysicsList", m_physics_lists_name="NEXO");
}

DetSim0Svc::~DetSim0Svc()
{

}

bool
DetSim0Svc::initialize()
{
    MgrOfAnaElem& mgr = MgrOfAnaElem::instance();
    // register
    if (m_ana_list.size() == 0) {
        // m_ana_list.push_back("NormalAnaMgr");
    }
    // TODO
    // user can create the Ana Element more Early
    SniperPtr<DetSimAlg> detsimalg(getScope(),"DetSimAlg");
    if (detsimalg.invalid()) {
        LogError << "Can't Load DetSimAlg" << std::endl;
        return false;
    }
    for (std::vector<std::string>::iterator it = m_ana_list.begin();
            it != m_ana_list.end();
            ++it) {
        ToolBase* t = 0;
        // find the tool first
        t = detsimalg->findTool(*it);
        //
        // create the tool if not exist
        if (not t) {
            t = detsimalg->createTool(*it);
        }
        //
        // register the tool into MgrOfAnaElem
        if (t) {
            IAnalysisElement* ae = dynamic_cast<IAnalysisElement*>(t);
            if (ae) {
                MgrOfAnaElem::instance().reg(*it, ae);
                continue;
            }
        } 
        LogError << "Can't Load Tool " << *it << std::endl;
        return false;
    }

    return true;
}

bool
DetSim0Svc::finalize()
{
    return true;
}

G4VUserDetectorConstruction* 
DetSim0Svc::createDetectorConstruction()
{
    nEXODetectorConstruction* dc = new nEXODetectorConstruction;
    return dc;
}

G4VUserPhysicsList* 
DetSim0Svc::createPhysicsList()
{
    if (m_physics_lists_name=="NEXO") {
        EXOPhysicsList* phylist = new EXOPhysicsList();
        return phylist;
    }
    G4PhysListFactory *physListFactory = new G4PhysListFactory();
    G4VUserPhysicsList *physicsList =
        physListFactory->GetReferencePhysList(m_physics_lists_name);
    assert(physicsList);
    return physicsList;
}

G4VUserPrimaryGeneratorAction* 
DetSim0Svc::createPrimaryGenerator()
{
    return new nEXOPrimaryGeneratorAction();
}

G4UserRunAction*  
DetSim0Svc::createRunAction() 
{
    return new nEXORunAction();
}

G4UserEventAction*  
DetSim0Svc::createEventAction() 
{
    return new nEXOEventAction();
}

G4UserStackingAction*  
DetSim0Svc::createStackingAction() 
{
    return new nEXOStackingAction();
} 

G4UserTrackingAction*  
DetSim0Svc::createTrackingAction() 
{
    return new nEXOTrackingAction();
} 

G4UserSteppingAction*  
DetSim0Svc::createSteppingAction() 
{
    return new nEXOSteppingAction();
} 
