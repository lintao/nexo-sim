#include "nEXOTPCExternalsConstructor.hh"


class nEXOTPCExternalsMessenger : public nEXOModuleConstructorMessenger
{
private:
  nEXOTPCExternalsConstructor* fConstructor;

  G4UIcmdWithAnInteger* fTPCExternalsVersionCmd;
  G4UIcmdWithAnInteger* fHVConfigCmd;

public:
  nEXOTPCExternalsMessenger(nEXOTPCExternalsConstructor* construct)
  : nEXOModuleConstructorMessenger(construct, "Control the geometry of the components outside of the nEXO TPC."),
    fConstructor(construct){

    fTPCExternalsVersionCmd = new G4UIcmdWithAnInteger(CommandName("setVersion"),this);
    fTPCExternalsVersionCmd->SetGuidance("The version of the TPC externals geometry.");
    fTPCExternalsVersionCmd->SetParameterName("setVersion",false);
    fTPCExternalsVersionCmd->SetDefaultValue(1);
    fTPCExternalsVersionCmd->AvailableForStates(G4State_PreInit);
    
    fHVConfigCmd = new G4UIcmdWithAnInteger(CommandName("setHVConfig"),this);
    fHVConfigCmd->SetGuidance("HV cable configuration, near or far.");
    fHVConfigCmd->SetParameterName("setHVConfig",false);
    fHVConfigCmd->SetDefaultValue(0);
    fHVConfigCmd->AvailableForStates(G4State_PreInit);
   

  };

  virtual ~nEXOTPCExternalsMessenger(){

    delete fTPCExternalsVersionCmd;
    delete fHVConfigCmd;
  };

  void SetNewValue(G4UIcommand *cmd, G4String val){
    nEXOModuleConstructorMessenger::SetNewValue(cmd,val);

    if( cmd == fTPCExternalsVersionCmd) {
         fConstructor->SetVersion(fTPCExternalsVersionCmd->GetNewIntValue(val));
    }
    if(cmd == fHVConfigCmd) {
      fConstructor->SetHVConfig(fHVConfigCmd->GetNewIntValue(val));
    }

  };
};

nEXOTPCExternalsConstructor::~nEXOTPCExternalsConstructor(){;}

void nEXOTPCExternalsConstructor::Init(void)
{
  SetMessenger(new nEXOTPCExternalsMessenger(this));

  //<<<<<<< .mine
  SetVersion(2);
  //=======
  // spherical design is default
  SetVersion(2);
  
  //>>>>>>> .r10221
  double radius = 955*cm;
  SetRadius1(radius);
  SetRadius2(radius);
  double length = 2*845*cm + radius;
  SetLength(length);

  fTPCVesselTranslation.setX(0.); fTPCVesselTranslation.setY(0.); fTPCVesselTranslation.setZ(0.);
  fTPCVesselMotherVolume = NULL;
  fTPCRadius = 650.*mm;
  fTPCHalfheight = 650.*mm;
  fTPCVesselSideThickness = 3.*mm;

  AddConstructor(new nEXOWaterTankConstructor("WaterShieldTank",this));
  
  AddConstructor(new nEXOOuterCryostatConstructor("OuterCryostat",this));
  AddConstructor(new nEXOInnerCryostatConstructor("InnerCryostat",this));

  AddConstructor(new nEXOOuterSphericalCryostatConstructor("OuterSphericalCryostat",this));
  AddConstructor(new nEXOInnerSphericalCryostatConstructor("InnerSphericalCryostat",this));
  AddConstructor(new nEXORepeaterBoxConstructor("RepeaterBox", this));

}

G4LogicalVolume* nEXOTPCExternalsConstructor::GetPiece(void)
{
  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************* nEXO TPC Externals ***************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4LogicalVolume *logicExternals = new G4LogicalVolume(new G4Cons(GetName(),
                                                                   0, GetRadius1(),
                                                                   0, GetRadius2(),
                                                                   GetLength()/2.,
                                                                   0, 360*deg),
                                                        FindMaterial("Rock"),
                                                        GetName());

  fTPCVesselMotherVolume = logicExternals;

  if(GetVersion() == 0)
    return GetPieceOld(logicExternals);

  if(GetVersion() == 1)
    return GetPieceV1(logicExternals);

  if(GetVersion() == 2)
    return GetPieceV2(logicExternals);
  

  return NULL;
}

G4LogicalVolume* nEXOTPCExternalsConstructor::GetPieceV1(G4LogicalVolume *logicExternals)
{
  // Water Tank Shield
  
  nEXOWaterTankConstructor& waterShieldTank = Get<nEXOWaterTankConstructor>("WaterShieldTank");
  waterShieldTank.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicWST = waterShieldTank.GetPiece();

  G4VPhysicalVolume* physWST = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,0),
                                                 logicWST,
                                                 waterShieldTank.GetName(),
                                                 logicExternals,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(waterShieldTank.GetName(),physWST);
  
  G4LogicalVolume* logicH2O = waterShieldTank.GetFillingPiece();
  double h2oOffset = waterShieldTank.GetVerticalOffset();

  G4VPhysicalVolume* physH2O = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,h2oOffset),
                                                 logicH2O,
                                                 waterShieldTank.GetName()+"/Water",
                                                 logicWST,
                                                 false,
                                                 0,fCheckOverlaps);
  
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physH2O->GetName(),physH2O);

  G4VisAttributes* WaterAtt = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));
  //WaterAtt->SetVisibility(true);
  //WaterAtt->SetForceSolid(true);  
  logicWST->SetVisAttributes(WaterAtt);
  logicH2O->SetVisAttributes(WaterAtt);

  // Outer Cryostat
  
  nEXOOuterCryostatConstructor& outerCryo = Get<nEXOOuterCryostatConstructor>("OuterCryostat");
  outerCryo.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicOC = outerCryo.GetPiece();

  G4VPhysicalVolume* physOC = new G4PVPlacement(0,
                                                G4ThreeVector(0,0,-outerCryo.GetMaxLength()/2.-h2oOffset),
                                                logicOC,
                                                outerCryo.GetName(),
                                                logicH2O,
                                                false,
                                                0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(outerCryo.GetName(),physOC);
  
  G4VisAttributes* ocAtt = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0));
  //WaterAtt->SetVisibility(true);
  //ocAtt->SetForceSolid(true);  
  logicOC->SetVisAttributes(ocAtt);

  G4LogicalVolume* logicVac = outerCryo.GetFillingPiece();
  G4VPhysicalVolume* physVac = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,outerCryo.GetThickness()),
                                                 logicVac,
                                                 outerCryo.GetName()+"/Vacuum",
                                                 logicOC,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physVac->GetName(),physVac);
  G4VisAttributes* vacAtt = new G4VisAttributes(G4Colour(0.0, 1.0, 1.0));
  //WaterAtt->SetVisibility(true);
  //vacAtt->SetForceSolid(true);  
  logicVac->SetVisAttributes(vacAtt);


  // Inner Cryostat

  nEXOInnerCryostatConstructor& innerCryo = Get<nEXOInnerCryostatConstructor>("InnerCryostat");
  innerCryo.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicIC = innerCryo.GetPiece();

  G4VPhysicalVolume* physIC = new G4PVPlacement(0,
                                                G4ThreeVector(0,0,outerCryo.GetRadius() - innerCryo.GetRadius()),
                                                logicIC,
                                                innerCryo.GetName(),
                                                logicVac,
                                                false,
                                                0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(innerCryo.GetName(),physIC);
  
  G4VisAttributes* icAtt = new G4VisAttributes(G4Colour(0.0, 0.0, 2.0));
  //WaterAtt->SetVisibility(true);
  //icAtt->SetForceSolid(true);  
  logicIC->SetVisAttributes(icAtt);

  G4LogicalVolume* logicHFE = innerCryo.GetFillingPiece();
  G4VPhysicalVolume* physHFE = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,innerCryo.GetThickness()),
                                                 logicHFE,
                                                 innerCryo.GetName()+"/HFE",
                                                 logicIC,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physHFE->GetName(),physHFE);
  G4VisAttributes* hfeAtt = new G4VisAttributes(G4Colour(0.0, 2.0, 2.0));
  //WaterAtt->SetVisibility(true);
  //hfeAtt->SetForceSolid(true);  
  logicHFE->SetVisAttributes(hfeAtt);

  // HV cable in near configuration
  
  //outer copper tube for cable
  G4Tubs* nearHVtube = new G4Tubs("nearHVtube",0,15.88*mm/2,700*mm,0,360.*deg);
  G4LogicalVolume* logicnearHVtube = new G4LogicalVolume(nearHVtube,FindMaterial("G4_Cu"),"near_HVtube"); 
  G4Torus* nearHVtubetor = new G4Torus("nearHVtubetor",0,15.88*mm/2,100.*mm,0,90.*deg);
  G4LogicalVolume* logicnearHVtubetor = new G4LogicalVolume(nearHVtubetor,FindMaterial("G4_Cu"),GetName());
  G4Tubs* nearHVtube2 = new G4Tubs("nearHVtube2",0,15.88*mm/2,125*mm,0,360.*deg);
  G4LogicalVolume* logicnearHVtube2 = new G4LogicalVolume(nearHVtube2,FindMaterial("G4_Cu"),GetName());
  //inner HDPE cable sheath
  G4Tubs* nearHVcable = new G4Tubs("near_HVcable",0,4.7625*mm,700*mm,0,360.*deg);
  G4LogicalVolume* logicnearHVcable = new G4LogicalVolume(nearHVcable,FindMaterial("HDPE"),GetName());
  G4Torus* nearHVcabletor = new G4Torus("nearHVtubetor",0,4.6725*mm/2,100.*mm,0,90.*deg);
  G4LogicalVolume* logicnearHVcabletor = new G4LogicalVolume(nearHVcabletor,FindMaterial("HDPE"),GetName());
  G4Tubs* nearHVcable2 = new G4Tubs("nearHVcable2",0,4.7625*mm/2,125*mm,0,360.*deg);
  G4LogicalVolume* logicnearHVcable2 = new G4LogicalVolume(nearHVcable2,FindMaterial("HDPE"),GetName());
  //feedthru piece
  G4Tubs* nearHVfeedouter = new G4Tubs("nearHVfeedouter",0,25.*mm,100.*mm,0,360*deg);
  G4LogicalVolume* logicnearHVfeedouter = new G4LogicalVolume(nearHVfeedouter,FindMaterial("G4_Cu"),"nearHVfeedouter");
  G4Tubs* nearHVfeedcore1 = new G4Tubs("nearHVfeedcore1",0,40.*mm,10.*mm,0,360*deg);
  G4LogicalVolume* logicnearHVfeedcore1 = new G4LogicalVolume(nearHVfeedcore1,FindMaterial("G4_TEFLON"),GetName());
  G4Tubs* nearHVfeedcore2 = new G4Tubs("nearHVfeedcore2",0,24.*mm,100.*mm,0,360*deg);
  G4LogicalVolume* logicnearHVfeedcore2 = new G4LogicalVolume(nearHVfeedcore2,FindMaterial("G4_TEFLON"),GetName());
  G4double cable_pos = 2200*mm;
  G4double cable_halflength = 1000*mm;
  G4RotationMatrix* rot = new G4RotationMatrix();
  rot->rotateY(90*deg);
  G4RotationMatrix* rot2 = new G4RotationMatrix();
  rot2->rotateX(90*deg);
  G4VPhysicalVolume* physnearHVtube = new G4PVPlacement(0, G4ThreeVector(cable_pos-1100*mm,0,1450*mm),logicnearHVtube, "near_HVtube",logicHFE,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVtubetor = new G4PVPlacement(rot2, G4ThreeVector(cable_pos-1200*mm,0,-cable_halflength+1750*mm),logicnearHVtubetor, "near_HVtubetor",logicHFE,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVtube2 = new G4PVPlacement(rot,G4ThreeVector(900*mm,0,-cable_halflength+1650*mm),logicnearHVtube2,"near_HVtube2",logicHFE,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVcable = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVcable,"near_HVcable",logicnearHVtube,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVcabletor = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVcabletor,"near_HVcabletor",logicnearHVtubetor,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVcable2 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVcable2,"near_HVcable2",logicnearHVtube2,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVfeedouter = new G4PVPlacement(rot,G4ThreeVector(700*mm,0,-cable_halflength+1650*mm),logicnearHVfeedouter,"near_HVfeedouter",logicHFE,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVfeedcore1 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVfeedcore1,"near_HVfeedcore1",logicnearHVfeedouter,false,0,fCheckOverlaps);
  G4VPhysicalVolume* physnearHVfeedcore2 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVfeedcore2,"near_HVfeedcore2",logicnearHVfeedcore1,false,0,fCheckOverlaps);

   
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVtube",physnearHVtube);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVtubetor->GetName(),physnearHVtubetor);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVtube2->GetName(),physnearHVtube2);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVcable",physnearHVcable);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVcabletor->GetName(),physnearHVcabletor);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVcable2->GetName(),physnearHVcable2);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVfeedouter",physnearHVfeedouter);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVfeedcore1->GetName(),physnearHVfeedcore1);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVfeedcore2->GetName(),physnearHVfeedcore2);
  

  // Others

  fTPCVesselMotherVolume = logicHFE;
  fTPCVesselTranslation.setZ((innerCryo.GetMaxLength() - 2*innerCryo.GetThickness())/2.);

  G4Region*  CryostatRegion = new G4Region("CryostatRegion");
  CryostatRegion->AddRootLogicalVolume(logicOC);

  
  return logicExternals;
}

G4LogicalVolume* nEXOTPCExternalsConstructor::GetPieceV2(G4LogicalVolume *logicExternals)
{
  // Water Tank Shield
  
  nEXOWaterTankConstructor& waterShieldTank = Get<nEXOWaterTankConstructor>("WaterShieldTank");
  waterShieldTank.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicWST = waterShieldTank.GetPiece();

  G4VPhysicalVolume* physWST = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,0),
                                                 logicWST,
                                                 waterShieldTank.GetName(),
                                                 logicExternals,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(waterShieldTank.GetName(),physWST);
  
  G4LogicalVolume* logicH2O = waterShieldTank.GetFillingPiece();
  double h2oOffset = waterShieldTank.GetVerticalOffset();

  G4VPhysicalVolume* physH2O = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,h2oOffset),
                                                 logicH2O,
                                                 waterShieldTank.GetName()+"/Water",
                                                 logicWST,
                                                 false,
                                                 0,fCheckOverlaps);
  
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physH2O->GetName(),physH2O);

  G4VisAttributes* WaterAtt = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));
  //WaterAtt->SetVisibility(true);
  //WaterAtt->SetForceSolid(true);  
  logicWST->SetVisAttributes(WaterAtt);
  logicH2O->SetVisAttributes(WaterAtt);

  // Outer Cryostat
  
  nEXOOuterSphericalCryostatConstructor& outerCryo = Get<nEXOOuterSphericalCryostatConstructor>("OuterSphericalCryostat");
  outerCryo.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicOC = outerCryo.GetPiece();

  G4VPhysicalVolume* physOC = new G4PVPlacement(0,
                                                G4ThreeVector(0,0,-h2oOffset),
                                                logicOC,
                                                outerCryo.GetName(),
                                                logicH2O,
                                                false,
                                                0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(outerCryo.GetName(),physOC);


  
  G4VisAttributes* ocAtt = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0));
  //WaterAtt->SetVisibility(true);
  //ocAtt->SetForceSolid(true);  
  logicOC->SetVisAttributes(ocAtt);

  nEXORepeaterBoxConstructor& RepBox = Get<nEXORepeaterBoxConstructor>("RepeaterBox");
  RepBox.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicRepBox = RepBox.GetPiece();
  G4VPhysicalVolume* physRepBox = new G4PVPlacement(0,G4ThreeVector(50*cm,0,outerCryo.GetRadius()+20.0*cm),logicRepBox,"RepeaterBox",logicH2O,false,0,fCheckOverlaps);

  G4LogicalVolume* logicRepBoxElec = RepBox.GetFillingPiece();
  G4VPhysicalVolume* physRepBoxElec = new G4PVPlacement(0,G4ThreeVector(50*cm,0,outerCryo.GetRadius()+20.0*cm),logicRepBoxElec,"RepBoxElectronics",logicH2O,false,0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("RepeaterBox",physRepBox);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("RepBoxElectronics",physRepBoxElec);

  G4VisAttributes* elecAtt = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
  logicRepBoxElec->SetVisAttributes(elecAtt);
							
  

  G4LogicalVolume* logicVac = outerCryo.GetFillingPiece();
  G4VPhysicalVolume* physVac = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,0),
                                                 logicVac,
                                                 outerCryo.GetName()+"/Vacuum",
                                                 logicOC,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physVac->GetName(),physVac);
  G4VisAttributes* vacAtt = new G4VisAttributes(G4Colour(0.0, 1.0, 1.0));
  //WaterAtt->SetVisibility(true);
  //vacAtt->SetForceSolid(true);  
  logicVac->SetVisAttributes(vacAtt);

  G4LogicalVolume* logicOS = outerCryo.GetSupportPiece();
  G4VPhysicalVolume* physOS = new G4PVPlacement(0,
                                                G4ThreeVector(0,0,1300*mm),
                                                logicOS,
                                                outerCryo.GetName()+"/Support",
                                                logicVac,
                                                false,
                                                0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physOS->GetName(),physOS);
  G4VisAttributes* osAtt = new G4VisAttributes(G4Colour(2.0, 0.0, 0.0));
  //WaterAtt->SetVisibility(true);
  //hfeAtt->SetForceSolid(true);  
  logicOS->SetVisAttributes(osAtt);

  // Inner Cryostat

  nEXOInnerSphericalCryostatConstructor& innerCryo = Get<nEXOInnerSphericalCryostatConstructor>("InnerSphericalCryostat");
  innerCryo.SetCheckOverlaps(fCheckOverlaps);
  G4LogicalVolume* logicIC = innerCryo.GetPiece();

  G4VPhysicalVolume* physIC = new G4PVPlacement(0,
                                                G4ThreeVector(0,0,innerCryo.GetOffsetToOuter()),
                                                logicIC,
                                                innerCryo.GetName(),
                                                logicVac,
                                                false,
                                                0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(innerCryo.GetName(),physIC);
  
  G4VisAttributes* icAtt = new G4VisAttributes(G4Colour(0.0, 0.0, 2.0));
  //WaterAtt->SetVisibility(true);
  //icAtt->SetForceSolid(true);  
  logicIC->SetVisAttributes(icAtt);

  G4LogicalVolume* logicHFE = innerCryo.GetFillingPiece();
  G4VPhysicalVolume* physHFE = new G4PVPlacement(0,
                                                 G4ThreeVector(0,0,0),
                                                 logicHFE,
                                                 innerCryo.GetName()+"/HFE",
                                                 logicIC,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physHFE->GetName(),physHFE);
  G4VisAttributes* hfeAtt = new G4VisAttributes(G4Colour(0.0, 2.0, 2.0));
  //WaterAtt->SetVisibility(true);
  //hfeAtt->SetForceSolid(true);  
  logicHFE->SetVisAttributes(hfeAtt);  

  //HV cable

  if(GetConfig() == 0)
    {
      G4Tubs* nearHVtube = new G4Tubs("nearHVtube",0,15.88*mm/2,700*mm,0,360.*deg);
      G4LogicalVolume* logicnearHVtube = new G4LogicalVolume(nearHVtube,FindMaterial("G4_Cu"),"near_HVtube");
      G4Torus* nearHVtubetor = new G4Torus("nearHVtubetor",0,15.88*mm/2,100.*mm,0,90.*deg);
      G4LogicalVolume* logicnearHVtubetor = new G4LogicalVolume(nearHVtubetor,FindMaterial("G4_Cu"),GetName());
  //inner HDPE cable sheath                                                                                                          
      G4Tubs* nearHVcable = new G4Tubs("near_HVcable",0,4.7625*mm,700*mm,0,360.*deg);
      G4LogicalVolume* logicnearHVcable = new G4LogicalVolume(nearHVcable,FindMaterial("HDPE"),"near_HVcable");
      G4Torus* nearHVcabletor = new G4Torus("nearHVtubetor",0,4.6725*mm,100.*mm,0,90.*deg);
      G4LogicalVolume* logicnearHVcabletor = new G4LogicalVolume(nearHVcabletor,FindMaterial("HDPE"),GetName());
  //feedthru piece                                                                                                                   
      G4Tubs* nearHVfeedouter = new G4Tubs("nearHVfeedouter",0*mm,25.*mm,50.*mm,0,360*deg);
      G4LogicalVolume* logicnearHVfeedouter = new G4LogicalVolume(nearHVfeedouter,FindMaterial("G4_Cu"),"nearHVfeedouter");
      G4Tubs* nearHVfeedcore2 = new G4Tubs("nearHVfeedcore2",0,24.*mm,50.*mm,0,360*deg);
      G4LogicalVolume* logicnearHVfeedcore2 = new G4LogicalVolume(nearHVfeedcore2,FindMaterial("G4_TEFLON"),"near_HVfeedcore2");

      //physical volume placement
      G4RotationMatrix* rot = new G4RotationMatrix();
      rot->rotateY(90*deg);
      G4RotationMatrix* rot2 = new G4RotationMatrix();
      rot2->rotateX(90*deg);
      G4VPhysicalVolume* physnearHVtube = new G4PVPlacement(0, G4ThreeVector(fTPCRadius+200*mm,0,fTPCHalfheight-300*mm),logicnearHVtube, "near_HVtube",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physnearHVtubetor = new G4PVPlacement(rot2, G4ThreeVector(fTPCRadius+100*mm,0,fTPCHalfheight-1000*mm),logicnearHVtubetor, "near_HVtubetor",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physnearHVcable = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVcable,"near_HVcable",logicnearHVtube,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physnearHVcabletor = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVcabletor,"near_HVcabletor",logicnearHVtubetor,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physnearHVfeedouter = new G4PVPlacement(rot,G4ThreeVector(fTPCRadius+54*mm,0,fTPCHalfheight-1100*mm),logicnearHVfeedouter,"near_HVfeedouter",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physnearHVfeedcore2 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicnearHVfeedcore2,"near_HVfeedcore2",logicnearHVfeedouter,false,0,fCheckOverlaps);

      
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVtube",physnearHVtube);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVtubetor->GetName(),physnearHVtubetor);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVcable",physnearHVcable);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physnearHVcabletor->GetName(),physnearHVcabletor);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVfeedouter",physnearHVfeedouter);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("near_HVfeedcore2",physnearHVfeedcore2);
    }

  //far HV cable configuration 
  if(GetConfig() == 1)
    {
      G4Tubs* farHVtube = new G4Tubs("farHVtube",0,15.88/2*mm,500*mm,0,360*deg); //400*mm
      G4LogicalVolume* logicfarHVtube = new G4LogicalVolume(farHVtube,FindMaterial("G4_Cu"),"far_HVtube");
      G4Torus* farHVtorus1 = new G4Torus("farHVtorus1",0,15.88/2*mm,100*mm,0,90*deg);
      G4LogicalVolume* logicfarHVtorus1 = new G4LogicalVolume(farHVtorus1,FindMaterial("G4_Cu"),GetName());
      G4Tubs* farHVtube2 = new G4Tubs("farHVtube2",0,15.88/2*mm,500*mm,0,360*deg); //400*mm
      G4LogicalVolume* logicfarHVtube2 = new G4LogicalVolume(farHVtube2,FindMaterial("G4_Cu"),"farHVtube2");
      G4Tubs* farHVcable = new G4Tubs("farHVcable",0,4.7625*mm,500*mm,0,360.*deg);
      G4LogicalVolume* logicfarHVcable = new G4LogicalVolume(farHVcable,FindMaterial("HDPE"),"far_HVcable");
      G4Torus* farHVcabletor1 = new G4Torus("farHVcabletor1",0,4.7625*mm,100*mm,0,90*deg);
      G4LogicalVolume* logicfarHVcabletor1 = new G4LogicalVolume(farHVcabletor1,FindMaterial("HDPE"),GetName());
      G4Tubs* farHVcable2 = new G4Tubs("farHVcable2",0,4.7625*mm,500*mm,0,360.*deg);
      G4LogicalVolume* logicfarHVcable2 = new G4LogicalVolume(farHVcable2,FindMaterial("HDPE"),"farHVcable2");
      G4Tubs* farHVfeedouter = new G4Tubs("farHVfeedouter",0,25.*mm,150.*mm,0,360*deg);
      G4LogicalVolume* logicfarHVfeedouter = new G4LogicalVolume(farHVfeedouter,FindMaterial("G4_Cu"),"farHVfeedouter");
      G4Tubs* farHVfeedcore2 = new G4Tubs("farHVfeedcore2",0,24.*mm,150.*mm,0,360*deg);
      G4LogicalVolume* logicfarHVfeedcore2 = new G4LogicalVolume(farHVfeedcore2,FindMaterial("G4_TEFLON"),"far_HVfeedcore2");

      G4RotationMatrix* rot = new G4RotationMatrix();
      rot->rotateY(45*deg);
      G4RotationMatrix* rot2 = new G4RotationMatrix();
      rot2->rotateY(-45*deg);
      G4RotationMatrix* rot3 = new G4RotationMatrix();
      rot3->rotateX(90*deg);
      rot3->rotateZ(45*deg);
      G4VPhysicalVolume* physfarHVtube = new G4PVPlacement(rot2,G4ThreeVector(fTPCRadius+450*mm,0,fTPCHalfheight-750*mm),logicfarHVtube,"far_HVtube",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVtorus1 = new G4PVPlacement(rot3,G4ThreeVector(fTPCRadius+730*mm,0,fTPCHalfheight-330*mm),logicfarHVtorus1,"farHVtorus1",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVtube2 = new G4PVPlacement(rot,G4ThreeVector(fTPCRadius+470*mm,0,fTPCHalfheight+70*mm),logicfarHVtube2,"farHVtube2",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVcable = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicfarHVcable,"far_HVcable",logicfarHVtube,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVcabletor1 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicfarHVcabletor1,"farHVcabletor1",logicfarHVtorus1,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVcable2 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicfarHVcable2,"farHVcable2",logicfarHVtube2,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVfeedouter = new G4PVPlacement(rot2,G4ThreeVector(fTPCRadius+54*mm,0,fTPCHalfheight-1150*mm),logicfarHVfeedouter,"farHV_feedouter",logicHFE,false,0,fCheckOverlaps);
      G4VPhysicalVolume* physfarHVfeedcore2 = new G4PVPlacement(0,G4ThreeVector(0,0,0),logicfarHVfeedcore2,"far_HVfeedcore2",logicfarHVfeedouter,false,0,fCheckOverlaps);

      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("far_HVtube",physfarHVtube);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physfarHVtorus1->GetName(),physfarHVtorus1);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("farHVtube2",physfarHVtube2);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("far_HVcable",physfarHVcable);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physfarHVcabletor1->GetName(),physfarHVcabletor1);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("farHVcable2",physfarHVcable2);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(physfarHVfeedouter->GetName(),physfarHVfeedouter);
      nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume("far_HVfeedcore2",physfarHVfeedcore2);
    }
  // Others

  fTPCVesselMotherVolume = logicHFE;
  //fTPCVesselTranslation.setZ((innerCryo.GetMaxLength() - 2*innerCryo.GetThickness())/2.);

  G4Region*  CryostatRegion = new G4Region("CryostatRegion");
  CryostatRegion->AddRootLogicalVolume(logicOC);

  
  return logicExternals;
}

G4LogicalVolume* nEXOTPCExternalsConstructor::GetPieceOld(G4LogicalVolume *logicExternals)
{

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//*************** SurroundRock *******************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String surroundRockName = GetName() + "/SurroundRock";
  G4double surroundRockR = 955*cm;
  G4double surroundRockH = 845*cm;  //Full height = 1690 cm
  G4LogicalVolume* logicSurroundRock = new G4LogicalVolume(new G4Tubs(surroundRockName,
                                                                      0, surroundRockR,
                                                                      surroundRockH,
                                                                      0, 360*deg),
                                                           FindMaterial("Rock"),
                                                           surroundRockName);

  G4double surroundRockPlaceZ = -110*cm;
  G4VPhysicalVolume* physSurroundRock = new G4PVPlacement(0,
                                                          G4ThreeVector(0,0,surroundRockPlaceZ),
                                                          logicSurroundRock,
                                                          surroundRockName,
                                                          logicExternals,
                                                          false,
                                                          0,fCheckOverlaps);
  
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(surroundRockName,physSurroundRock);

  G4VisAttributes* MineAtt = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  MineAtt->SetVisibility(true);
  MineAtt->SetForceWireframe(true);
  logicSurroundRock->SetVisAttributes(MineAtt);


  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************* Shotcrete Coating ****************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String shotcreteName = GetName() + "/Shotcrete";
  G4double shotcreteR = 755*cm;
  G4double shotcreteH = 735*cm;  //Full height = 1470 cm
  G4LogicalVolume* logicShotcrete =  new G4LogicalVolume(new G4Tubs(shotcreteName,
                                                                    0, shotcreteR,
                                                                    shotcreteH,
                                                                    0, 360*deg),
                                                         FindMaterial("ShotCrete"),
                                                         shotcreteName);

  G4double shotcretePlaceZ = surroundRockH - shotcreteH;
  G4VPhysicalVolume* physShotcrete = new G4PVPlacement(0,
                                                       G4ThreeVector(0,0,shotcretePlaceZ),
                                                       logicShotcrete,
                                                       shotcreteName,
                                                       logicSurroundRock,
                                                       false,
                                                       0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(shotcreteName,physShotcrete);
  logicShotcrete->SetVisAttributes(MineAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//******************* Floor **********************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String floorName = GetName() + "/Floor";
  G4double floorR = shotcreteR;
  G4double floorH = 110*cm;  //Full height = 2.2m
  G4LogicalVolume* logicFloor = new G4LogicalVolume(new G4Tubs(floorName,
                                                               0, floorR,
                                                               floorH,
                                                               0, 360*deg),
                                                    FindMaterial("Concrete"),
                                                    floorName);

  G4double floorPlaceZ = floorH - surroundRockH;
  G4VPhysicalVolume* physFloor = new G4PVPlacement(0,
                                                   G4ThreeVector(0,0,floorPlaceZ),
                                                   logicFloor,
                                                   floorName,
                                                   logicSurroundRock,
                                                   false,
                                                   0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(floorName,physFloor);
  logicFloor->SetVisAttributes(MineAtt);

  //***************************************************//
  // Two separate water shields for calculating fluxes //
  // halfway through the water                         //
  //***************************************************//

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************* Outer Water Shield 1 *************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String outerWater1Name = GetName() + "/OuterWater1";
  G4double outerWater1R = 735.*cm;
  //  G4double outerWaterThick = 148.5*cm;  // Full thickness 297 cm
  G4double outerWater1H = 735.*cm;  // Full height = 1470 cm

  G4LogicalVolume* logicOuterWater1 = new G4LogicalVolume(new G4Tubs(outerWater1Name,
                                                                     0, outerWater1R,
                                                                     outerWater1H,
                                                                     0, 360*deg),
                                                          FindMaterial("G4_WATER"),
                                                          outerWater1Name);

  G4double outerWaterPlace1Z = shotcreteH - outerWater1H;
  G4VPhysicalVolume* physOuterWater1 = new G4PVPlacement(0,
                                                         G4ThreeVector(0,0,outerWaterPlace1Z),
                                                         logicOuterWater1,
                                                         outerWater1Name,
                                                         logicShotcrete,
                                                         false,
                                                         0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(outerWater1Name,physOuterWater1);
  G4VisAttributes* WaterAtt = new G4VisAttributes(G4Colour(0.0, 0.0, 1.0));
  WaterAtt->SetVisibility(true);
  //WaterAtt->SetForceSolid(true);  
  logicOuterWater1->SetVisAttributes(WaterAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************* Outer Water Shield 2 *************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String outerWater2Name = GetName() + "/OuterWater2";
  
  G4double HDPETankThick = 25.*cm;
  
  G4double innerWater1R = 440.*cm;
  G4double innerWater1H = 440.*cm;
  
  G4double HDPETankR = innerWater1R + HDPETankThick;
  G4double HDPETankH = innerWater1H + HDPETankThick;

  //halfway through OW tank

  G4double outerWater2R = (outerWater1R + HDPETankR)/2.;
  G4double outerWater2H = (outerWater1H + HDPETankH)/2.;

  G4LogicalVolume* logicOuterWater2 = new G4LogicalVolume(new G4Tubs(outerWater2Name,
                                                                     0, outerWater2R,
                                                                     outerWater2H,
                                                                     0, 360*deg),
                                                          FindMaterial("G4_WATER"),
                                                          outerWater2Name);

  G4double outerWaterPlace2Z = 0;
  G4VPhysicalVolume* physOuterWater2 = new G4PVPlacement(0,
                                                         G4ThreeVector(0,0,outerWaterPlace2Z),
                                                         logicOuterWater2,
                                                         outerWater2Name,
                                                         logicOuterWater1,
                                                         false,
                                                         0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(outerWater2Name,physOuterWater2);
  logicOuterWater2->SetVisAttributes(WaterAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//****************** HDPE Tank *******************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String HDPETankName = GetName() + "/HDPETank";
  G4LogicalVolume* logicHDPETank = new G4LogicalVolume(new G4Tubs(HDPETankName,
                                                                  0, HDPETankR,
                                                                  HDPETankH,
                                                                  0, 360*deg),
                                                       FindMaterial("HDPE"),
                                                       HDPETankName);

  G4double HDPETankPlaceZ = 0.;
  G4VPhysicalVolume* physHDPETank = new G4PVPlacement(0,
                                                      G4ThreeVector(0,0, HDPETankPlaceZ),
                                                      logicHDPETank,
                                                      HDPETankName,
                                                      logicOuterWater2,
                                                      false,
                                                      0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(HDPETankName,physHDPETank);
  G4VisAttributes* HdpeAtt = new G4VisAttributes(G4Colour(1.0, 0.0, 0.0));
  HdpeAtt->SetVisibility(true);
  HdpeAtt->SetForceWireframe(true);
  //HdpeAtt->SetForceSolid(true);
  logicHDPETank->SetVisAttributes(HdpeAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************ Inner Water Shield 1 **************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;  

  G4String innerWater1Name = GetName() + "/InnerWater1";
  G4LogicalVolume* logicInnerWater1 = new G4LogicalVolume(new G4Tubs(innerWater1Name,
                                                                     0, innerWater1R,
                                                                     innerWater1H,
                                                                     0, 360*deg),
                                                          FindMaterial("G4_WATER"),
                                                          innerWater1Name);

  G4double innerWater1PlaceZ = 0.;
  G4VPhysicalVolume* physInnerWater1 = new G4PVPlacement(0,
                                                         G4ThreeVector(0,0,innerWater1PlaceZ),
                                                         logicInnerWater1,
                                                         innerWater1Name,
                                                         logicHDPETank,
                                                         false,
                                                         0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(innerWater1Name,physInnerWater1);
  logicInnerWater1->SetVisAttributes(WaterAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************ Inner Water Shield 2 **************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;  

  G4String innerWater2Name = GetName() + "/InnerWater2";
  G4double innerWater2R = 326.*cm;
  G4double innerWater2H = 326.*cm;

  G4LogicalVolume* logicInnerWater2 = new G4LogicalVolume(new G4Tubs(innerWater2Name,
                                                                     0, innerWater2R,
                                                                     innerWater2H,
                                                                     0, 360*deg),
                                                          FindMaterial("G4_WATER"),
                                                          innerWater2Name);

  G4double innerWater2PlaceZ = 0.;
  G4VPhysicalVolume* physInnerWater2 = new G4PVPlacement(0,
                                                         G4ThreeVector(0,0,innerWater2PlaceZ),
                                                         logicInnerWater2,
                                                         innerWater2Name,
                                                         logicInnerWater1,
                                                         false,
                                                         0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(innerWater2Name,physInnerWater2);
  logicInnerWater2->SetVisAttributes(WaterAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//*************** Outer Cryostat *****************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String outerCryoName = GetName() + "/OuterCryo";
  G4double outerCryoR = 187*cm;
  G4double outerCryoH = 187*cm;
  G4LogicalVolume* logicOuterCryo = new G4LogicalVolume(new G4Tubs(outerCryoName,
                                                                   0, outerCryoR,
                                                                   outerCryoH,
                                                                   0, 360*deg),
                                                        FindMaterial("G4_Cu"),
                                                        outerCryoName);

  G4VPhysicalVolume* physOuterCryo = new G4PVPlacement(0,
                                                       G4ThreeVector(0,0,0),
                                                       logicOuterCryo,
                                                       outerCryoName,
                                                       logicInnerWater2,
                                                       false,
                                                       0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(outerCryoName,physOuterCryo);
  G4VisAttributes* CryoAtt = new G4VisAttributes(G4Colour(1.0, 0.0, 0.0));
  CryoAtt->SetVisibility(true);
  logicOuterCryo->SetVisAttributes(CryoAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//********** Vacuum Between Cryostats ************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String vacuumGapName = GetName() + "/VacuumGap";
  G4double vesselThickness = 4.86*cm;

  G4double outerCryoThick = vesselThickness;

  G4double vacuumGapR = outerCryoR - outerCryoThick;
  G4double vacuumGapH = outerCryoH - outerCryoThick;

  G4LogicalVolume* logicVacuumGap = new G4LogicalVolume(new G4Tubs(vacuumGapName,
                                                                   0, vacuumGapR,
                                                                   vacuumGapH,
                                                                   0,360*deg),
                                                        FindMaterial("G4_Galactic"),
                                                        vacuumGapName);

  G4VPhysicalVolume* physVacuumGap = new G4PVPlacement(0,
                                                       G4ThreeVector(),
                                                       logicVacuumGap,
                                                       vacuumGapName,
                                                       logicOuterCryo,
                                                       false,
                                                       0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(vacuumGapName,physVacuumGap);
  logicVacuumGap->SetVisAttributes(CryoAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//*************** Inner Cryostat *****************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String innerCryoName = GetName() + "/InnerCryo";
  G4double vacuumGapThick = 28.7*cm;

  G4double innerCryoR = vacuumGapR - vacuumGapThick;
  G4double innerCryoH = vacuumGapH - vacuumGapThick;

  G4LogicalVolume* logicInnerCryo = new G4LogicalVolume(new G4Tubs(innerCryoName,
                                                                   0, innerCryoR,
                                                                   innerCryoH,
                                                                   0, 360*deg),
                                                        FindMaterial("G4_Cu"),
                                                        innerCryoName);

  G4VPhysicalVolume* physInnerCryo = new G4PVPlacement(0,
                                                       G4ThreeVector(),
                                                       logicInnerCryo,
                                                       innerCryoName,
                                                       logicVacuumGap,
                                                       false,
                                                       0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(innerCryoName,physInnerCryo);
  logicInnerCryo->SetVisAttributes(CryoAtt);
  
  G4cout << "//************************************************//" << G4endl;
  G4cout << "//******************** HFE ***********************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String HFEName = GetName() + "/HFE";
  G4double innerCryoThick = vesselThickness;

  G4double HFER = innerCryoR - innerCryoThick;
  G4double HFEH = innerCryoH - innerCryoThick;

  G4LogicalVolume* logicHFE = new G4LogicalVolume(new G4Tubs(HFEName,
                                                             0, HFER,
                                                             HFEH,
                                                             0, 360*deg),
                                                  FindMaterial("HFE"),
                                                  HFEName);

  G4VPhysicalVolume* physHFE = new G4PVPlacement(0,
                                                 G4ThreeVector(),
                                                 logicHFE,
                                                 HFEName,
                                                 logicInnerCryo,
                                                 false,
                                                 0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(HFEName,physHFE);
  logicHFE->SetVisAttributes(CryoAtt);

  fTPCVesselMotherVolume = logicHFE;


  //***************************************************//
  // Build air cavity based on dimensions of cryopit   //
  //***************************************************//
  
  G4cout << "//************************************************//" << G4endl;
  G4cout << "//******** Air Cavity Surrounding Rock  **********//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String rockDomeName = GetName() + "/RockDome";
  G4double rockDomeR = surroundRockR;
 
  G4LogicalVolume* logicRockDome = new G4LogicalVolume(new G4Sphere(rockDomeName,
                                                                    0, rockDomeR,
                                                                    0, 360*deg, 0, 90*deg),
                                                       FindMaterial("Rock"),
                                                       rockDomeName);

  G4VPhysicalVolume* physRockDome = new G4PVPlacement(0,
                                                      G4ThreeVector(0,0,surroundRockPlaceZ+surroundRockH),
                                                      logicRockDome,
                                                      rockDomeName,
                                                      logicExternals,
                                                      false,
                                                      0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(rockDomeName,physRockDome);

  logicRockDome->SetVisAttributes(MineAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//************* Air Cavity Coating ***************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String SCDomeName = GetName() + "/ShotDome";
  G4double SCDomeR = shotcreteR;

  G4LogicalVolume* logicShotDome = new G4LogicalVolume(new G4Sphere(SCDomeName,
                                                                    0, SCDomeR,
                                                                    0, 360*deg, 0, 90*deg),
                                                       FindMaterial("ShotCrete"),
                                                       SCDomeName);
  
  G4VPhysicalVolume* physShotDome = new G4PVPlacement(0,
                                                      G4ThreeVector(0,0,0),
                                                      logicShotDome,
                                                      SCDomeName,
                                                      logicRockDome,
                                                      false,
                                                      0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(SCDomeName,physShotDome);
  logicShotDome->SetVisAttributes(MineAtt);

  G4cout << "//************************************************//" << G4endl;
  G4cout << "//***************** Air Cavity *******************//" << G4endl;
  G4cout << "//************************************************//" << G4endl;

  G4String airDomeName = GetName() + "/AirDome";
  G4double airDomeR = outerWater1R;

  G4LogicalVolume* logicAirDome = new G4LogicalVolume(new G4Sphere(airDomeName,
                                                    0, airDomeR,
                                                    0, 360*deg, 0, 90*deg),
                                       FindMaterial("G4_AIR"),
                                       airDomeName);

  G4VPhysicalVolume* physAirDome = new G4PVPlacement(0,
                                                     G4ThreeVector(0,0,0),
                                                     logicAirDome,
                                                     airDomeName,
                                                     logicShotDome,
                                                     false,
                                                     0,fCheckOverlaps);
  nEXOSimplePhysVolManager::GetInstance()->AddPhysicalVolume(airDomeName,physAirDome);
  G4VisAttributes* AirAtt = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  AirAtt->SetVisibility(true);
  //AirAtt->SetForceSolid(true);
  logicAirDome->SetVisAttributes(AirAtt);

  G4Region*  CryostatRegion = new G4Region("CryostatRegion");
  CryostatRegion->AddRootLogicalVolume(logicOuterCryo);


  return logicExternals;

}
