#include "nEXOTPCVesselConstructor.hh"
#include "G4UnionSolid.hh"

#include "nEXOTPCVesselModelConstructor.hh"
#include "nEXOTPCVesselCylFlatEndConstructor.hh"
#include "nEXOTPCVesselCyl2RoundEndConstructor.hh"
#include "nEXOTPCVesselSodaCanConstructor.hh"

nEXOTPCVesselConstructor::~nEXOTPCVesselConstructor(){;}

void nEXOTPCVesselConstructor::Init(void)
{
  SetMessenger(new nEXOTPCVesselMessenger(this));
  
  InitModels();
  
  fModel = "AllenCylFlatEnd"; //"BartoszekCylFlatEnd";
}

void nEXOTPCVesselConstructor::InitModels(void)
{
  // Initialize all available TPC vessel models

  // Dimensions of the 1st cylindrical flat end model (Bartoszek)
  nEXOTPCVesselCylFlatEndConstructor* cylBartFlatEnd = new nEXOTPCVesselCylFlatEndConstructor("BartoszekCylFlatEnd",this);
  cylBartFlatEnd->SetThickness(0.3*cm);
  cylBartFlatEnd->SetEndsThickness(0.66*cm);
  cylBartFlatEnd->SetInnerRadius(65.*cm);
  cylBartFlatEnd->SetInnerLength(2*65.*cm);
  fTPCVesselModelConstructor[cylBartFlatEnd->GetLocalName()] = cylBartFlatEnd;
  
  // Allen's revised dimension of the cylindrical flat end model from Bartoszek
  nEXOTPCVesselCylFlatEndConstructor* cylAllenFlatEnd = new nEXOTPCVesselCylFlatEndConstructor("AllenCylFlatEnd",this);
 
  double thickness = 2*mm;
  double diameter = 1294*mm;
  double ends = 19.18*mm; // to satisfy total mass
  double halflength = 650*mm;

  double radius = diameter/2.;
  double length = 2*(halflength + ends);
  
  cylAllenFlatEnd->SetThickness(thickness);
  cylAllenFlatEnd->SetEndsThickness(ends);
  cylAllenFlatEnd->SetInnerRadius(radius - thickness);
  cylAllenFlatEnd->SetInnerLength(length - 2*ends);

  fTPCVesselModelConstructor[cylAllenFlatEnd->GetLocalName()] = cylAllenFlatEnd;

  // Vance's model with two rounds at the caps
  nEXOTPCVesselCyl2RoundEndConstructor* cylVance2RoundEnd = new nEXOTPCVesselCyl2RoundEndConstructor("VanceCyl2RoundEnd",this);
  
  //G4double length(652.29*mm), radius(1306/2.*mm), thickness(3.*mm);
  //G4double offset(2.*mm), transition(105.*mm), small(125.*mm), smallthick(8.*mm);
  //G4double cap(1142.52*mm), capthick(6.*mm);
  
  cylVance2RoundEnd->SetDimensions(652.29*mm,1306/2.*mm,3.*mm,2.*mm,107.*mm,125.*mm,8.*mm,1142.52*mm,6.*mm);

  fTPCVesselModelConstructor[cylVance2RoundEnd->GetLocalName()] = cylVance2RoundEnd;

  // Soda can model with flat top
  nEXOTPCVesselSodaCanConstructor* sodaCan = new nEXOTPCVesselSodaCanConstructor("SodaCan",this);

  // Allen + Tamer drawings
  thickness = 3.*mm; // based on slide in Allen's email for ~143 kg
  G4double top = 12.4*mm; // based on Allen's Vance's dimensions of ~150.5kg on top lid
  G4double bottom = 21.*mm;
  length = 2*650.*mm + bottom + top;
  radius = 656.*mm;

  std::vector<G4double> innerAxial(3,0.);
  std::vector<G4double> innerRadial(3,0.);
  innerAxial[2] = -7.5*mm; innerRadial[2] = 0.*mm;
  innerAxial[1] = -100.*mm; innerRadial[1] = 415.*mm;
  innerAxial[0] = -190.*mm; innerRadial[0] = 455.*mm;
    
  std::vector<G4double> outerAxial(4,0.);
  std::vector<G4double> outerRadial(4,0.);
  outerAxial[0] = innerAxial[0]; outerRadial[0] = 490.*mm;
  outerAxial[1] = -130.*mm; outerRadial[1] = 535.*mm;
  outerAxial[2] = -55.*mm; outerRadial[2] = 650.*mm;
  outerAxial[3] = -30.*mm; outerRadial[3] = radius;

  sodaCan->SetDimensions(length, radius, thickness, top, bottom, innerAxial, innerRadial, outerAxial, outerRadial);
  
  fTPCVesselModelConstructor[sodaCan->GetLocalName()] = sodaCan;
  
}

G4LogicalVolume* nEXOTPCVesselConstructor::GetPiece(void)
{
  if(fTPCVesselModelConstructor.find(fModel) == fTPCVesselModelConstructor.end())
  {
    G4cout << "TPC vessel model: " << fModel << " is not supported. Will quit simulation..." << G4endl;
    exit(1);
  }

  G4cout << "//################################################//" << G4endl;
  G4cout << "//######## TPC Vessel Model: " << fTPCVesselModelConstructor.at(fModel)->GetLocalName() << G4endl;
  G4cout << "//################################################//" << G4endl;
  
  G4LogicalVolume* piece = fTPCVesselModelConstructor.at(fModel)->GetPiece();
  piece->SetName(GetName());

  return piece;
}

G4VSolid* nEXOTPCVesselConstructor::GetFillingShape(void)
{
  return fTPCVesselModelConstructor.at(fModel)->GetFillingShape();
}

double nEXOTPCVesselConstructor::GetInnerLength()
{
  return fTPCVesselModelConstructor.at(fModel)->GetInnerLength();
}

double nEXOTPCVesselConstructor::GetInnerTopRadius()
{
  return fTPCVesselModelConstructor.at(fModel)->GetInnerTopRadius();
}

double nEXOTPCVesselConstructor::GetInnerBottomRadius()
{
  return fTPCVesselModelConstructor.at(fModel)->GetInnerBottomRadius();
}

void nEXOTPCVesselConstructor::SetOpticalSurface(G4String volName)
{  
  //Optical Cu Surfaces
  const G4int nCuEntries = 32;
  //polycrystalline copper
  G4double copperPhotonEnergy[nCuEntries] =
            { 0.602*eV, 1.00*eV, 1.5*eV, 1.75*eV,
              1.90*eV, 2.40*eV, 2.80*eV, 3.4*eV,
              3.8*eV, 4.0*eV, 4.2*eV, 4.4*eV,
              4.6*eV, 4.8*eV, 5.0*eV, 5.2*eV,
              5.4*eV, 5.6*eV, 5.8*eV, 6.0*eV,
              6.5*eV, 7.0*eV, 7.5*eV, 8.0*eV,
              8.5*eV, 9.0*eV, 9.5*eV, 10*eV,
              11*eV, 12*eV, 13*eV, 14*eV };
  G4double copperRealRefractiveIndex[nCuEntries] =
            { 0.89, 0.433, 0.26, 0.214,
              0.214, 1.12, 1.17, 1.27,
              1.34, 1.34, 1.42, 1.49,
              1.52, 1.53, 1.47, 1.38,
              1.28, 1.18, 1.10, 1.04,
              0.958, 0.972, 1.01, 1.03,
              1.03, 1.03, 1.03, 1.04,
              1.07, 1.09, 1.08, 1.06 };
  G4double copperImaginaryRefractiveIndex[nCuEntries] =
            { 11.0, 8.46, 5.26, 4.24,
              3.67, 2.6,  2.36, 1.95,
              1.81, 1.72, 1.64, 1.64,
              1.67, 1.71, 1.78, 1.80,
              1.78, 1.74, 1.67, 1.59,
              1.37, 1.20, 1.09, 1.03,
              0.979, 0.921, 0.867, 0.818,
              0.754, 0.731, 0.724, 0.724 };

  G4OpticalSurface* tpcVesselRefOpSurface = new G4OpticalSurface("tpcVesselRefOpSurface");
  G4LogicalBorderSurface* tpcVesselRefSurface = NULL;
  tpcVesselRefSurface = new G4LogicalBorderSurface("tpcVesselRefSurface",
                                                   nEXOSimplePhysVolManager::GetInstance()->GetPhysicalVolume(volName),  // physTPCInternals, //physActiveLXe,
                                                   nEXOSimplePhysVolManager::GetInstance()->GetPhysicalVolume(GetName()),// physTPCVessel, //physiTPCVessel,
                                                   tpcVesselRefOpSurface);

  tpcVesselRefOpSurface->SetModel(unified);
  tpcVesselRefOpSurface->SetType(dielectric_metal);
  tpcVesselRefOpSurface->SetFinish(polished);

  G4MaterialPropertiesTable* tpcVesselRefOpticalMPT  = new G4MaterialPropertiesTable();
  tpcVesselRefOpticalMPT->AddProperty("REALRINDEX", copperPhotonEnergy, copperRealRefractiveIndex, nCuEntries);
  tpcVesselRefOpticalMPT->AddProperty("IMAGINARYRINDEX", copperPhotonEnergy, copperImaginaryRefractiveIndex, nCuEntries);

  tpcVesselRefOpSurface->SetMaterialPropertiesTable(tpcVesselRefOpticalMPT);
}

/*
G4LogicalVolume* nEXOTPCVesselConstructor::GetPiece(void)
{
  G4cout << "//################################################//" << G4endl;
  G4cout << "//################ TPC Vessel ####################//" << G4endl;
  G4cout << "//################################################//" << G4endl;

  G4LogicalVolume* logicTPC = 0;
  if(fOption == 0 || fOption == 2) {
    if(fOption == 2)
    {
      // Allen's 2nd model
      double thickness = 2*mm;
      double diameter = 1294*mm;
      double ends = 19.18*mm; // to satisfy total mass
      double radius = diameter/2.;
      double halflength = 650*mm;
  
      SetThickness(thickness);
      SetEndsThickness(ends);
      SetRadius1(radius);
      SetRadius2(radius);
      SetLength(2*(halflength + ends));
    }
    
    logicTPC =  new G4LogicalVolume(new G4Cons(GetName(),
                                    0, GetRadius1(),
                                    0, GetRadius2(),
                                    GetLength()/2.,
                                    0, 360*deg),
                                    FindMaterial("G4_Cu"),
                                    GetName());
  }
  else if(fOption == 1) {
    G4Tubs* mainTPC = new G4Tubs("mainTPC", 0, GetRadius2(), GetLength()/2., 0, 360*deg);
    G4Tubs* lotusTPC1 = new G4Tubs("lotusTPC1", GetRadius2(), GetRadius2()+2.0*cm, 3.1/2*cm, 0, 360*deg);
    G4Tubs* lotusTPC2 = new G4Tubs("lotusTPC2", (56-0.3)*cm, GetRadius2()+2.0*cm, 3.5/2*cm, 0, 360*deg);
    G4UnionSolid* partUnionTPC = new G4UnionSolid("main+tpc1", mainTPC, lotusTPC1, 0, G4ThreeVector(0, 0, -GetLength()/2.+3.1/2*cm));
    G4UnionSolid* solidTPC = new G4UnionSolid(GetName(), partUnionTPC, lotusTPC2, 0, G4ThreeVector(0, 0, -GetLength()/2.-3.5/2*cm));
    logicTPC = new G4LogicalVolume(solidTPC,
                                   FindMaterial("G4_Cu"),
                                   GetName());
  }

  G4VisAttributes* tpcAtt = new G4VisAttributes(G4Colour(0.0, 1.0, 1.0));
  tpcAtt->SetVisibility(true);
  //tpcAtt->SetForceSolid(true);
  logicTPC->SetVisAttributes(tpcAtt);

  return logicTPC;
}
*/
